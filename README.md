# learnPy
Learning Python

## install requirements

only test
```
pip install -r requirements.txt 
```

## tests

Tests are written under unittest and BDD style with behave. Run them with

```
python -m unittest
python -m behave
```