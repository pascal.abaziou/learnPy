import turtle
from math import sqrt
from random import random, randint


class Point:

    def __init__(self, x, y):
        self.x = x
        self.y = y



def draw_sierpinski(length,depth):
    if depth==0:
        pass
    else:
        draw_sierpinski(length/2,depth-1)
        t.fd(length/2)
        draw_sierpinski(length/2,depth-1)
        t.bk(length/2)
        t.left(60)
        t.fd(length/2)
        t.right(60)
        draw_sierpinski(length/2,depth-1)


window = turtle.Screen()
t = turtle.Turtle()
t.speed(10)

a = Point(0, 0)
b = Point(500, 0)
c = Point(250, 250 * sqrt(3))


for i in range(0, 3):
    t.fd(500)
    t.left(120)

t.penup()


p = Point(250, 250)
t.setx(p.x)
t.sety(p.y)
t.dot()


for _ in range(10000):
    r = randint(0, 2)

    if r == 0:
        x = p.x / 2
        y = p.y / 2
    elif r == 1:
        x = (p.x + b.x) / 2
        y = p.y / 2
    else:
        x = (p.x + c.x) / 2
        y = (p.y + c.y) / 2


    t.setx(x)
    t.sety(y)
    p = Point(x, y)

    t.dot()




window.exitonclick()
