monkeys = {}


op = {
    '+': lambda x, y: x + y,
    '-': (lambda x, y: x - y),
    '*': (lambda x, y: x * y),
    '/': (lambda x, y: x / y)
}


def solve(monkey):

    if monkey == "humn":
        return 'x'

    expr = monkeys[monkey]
    if expr.isdigit():
        return int(expr)
    e = expr.split(' ')

    return op[e[1]](solve(e[0]), solve(e[2]))


if __name__ == '__main__':

    while True:
        s = input()
        if s == "end":
            break
        m = s.split(":")
#        print(m[0])
#        print(m[1])
        monkeys[m[0]] = m[1][1:]

    a = solve()

    print(solve('root'))
