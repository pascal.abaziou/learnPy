def count_cross_word_in_puzzle(puzzle, word):
    def search_cross_from_position(x, y):
        if puzzle[x][y] != 'A':
            return False
        directions = [(-1, -1), (-1, 1), (1, -1), (1, 1)]
        for dx, dy in directions:
            d = 0
            # Manually rewritten the following line:
            if not (0 <= x + dx < len(puzzle) and 0 <= y + dy < len(puzzle[0]) and 0 <= x - dx < len(puzzle) and 0 <= y - dy < len(puzzle[0])):
                return False
            if puzzle[x + dx][y + dy] == 'M' and puzzle[x - dx][y - dy] == 'S' \
                or puzzle[x + dx][y + dy] == 'S' and puzzle[x - dx][y - dy] == 'M':
                d += 1
            if puzzle[x + dx][y - dy] == 'M' and puzzle[x - dx][y + dy] == 'S' \
                or puzzle[x + dx][y - dy] == 'S' and puzzle[x - dx][y + dy] == 'M':
                d += 1

            return d == 2

    count = 0
    for i in range(len(puzzle)):
        for j in range(len(puzzle[0])):
            if search_cross_from_position(i, j):
                count += 1

    return count

# Read the puzzle from the file
with open("day4.txt", "r") as f:
    puzzle = [list(line.strip()) for line in f.readlines()]

# Count occurrences of the cross word pattern
word = "MAS"
count = count_cross_word_in_puzzle(puzzle, word)
print(f"The cross word pattern {word} occurs {count} times in the puzzle.")
