
# Search for word XMAS in the input array in any direction (up, down, left, right, diagonal) from puzzle[][] and count how many times it occurs

def count_word_in_puzzle(puzzle, word):
    def search_from_position(x, y, dx, dy):
        for i in range(len(word)):
            if not (0 <= x < len(puzzle) and 0 <= y < len(puzzle[0]) and puzzle[x][y] == word[i]):
                return False
            x += dx
            y += dy
        return True

    directions = [(-1, 0), (1, 0), (0, -1), (0, 1), (-1, -1), (-1, 1), (1, -1), (1, 1)]
    count = 0

    for i in range(len(puzzle)):
        for j in range(len(puzzle[0])):
            for dx, dy in directions:
                if search_from_position(i, j, dx, dy):
                    count += 1

    return count

# Read the puzzle from the file
with open("day4.txt", "r") as f:
    puzzle = [list(line.strip()) for line in f.readlines()]

# Count occurrences of the word XMAS
word = "XMAS"
count = count_word_in_puzzle(puzzle, word)
print(f"The word {word} occurs {count} times in the puzzle.")
