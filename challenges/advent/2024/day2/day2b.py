f = open("day2.txt", "r")

safes = 0


for line in f.readlines():
    levels = list(map(int, line.split()))
    if all(x > y and abs(x-y) <= 3 for x, y in zip(levels, levels[1:])) or all(x < y and abs(x-y) <= 3 for x, y in zip(levels, levels[1:])):
        safes += 1

    # create all subsets of the list removing one element
    else:
        for i in range(len(levels)):
            subset = levels[:i] + levels[i+1:]
            if all(x > y and abs(x-y) <= 3 for x, y in zip(subset, subset[1:])) or all(x < y and abs(x-y) <= 3 for x, y in zip(subset, subset[1:])):
                safes += 1
                break
        
print(safes)
f.close()
