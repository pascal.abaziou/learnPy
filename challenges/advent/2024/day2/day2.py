f = open("day2.txt", "r")

safes = 0

for line in f.readlines():
    levels = list(map(int, line.split()))
    if all(x > y and abs(x-y) <= 3 for x, y in zip(levels, levels[1:])) or all(x < y and abs(x-y) <= 3 for x, y in zip(levels, levels[1:])):
        safes += 1
        
print(safes)
f.close()
