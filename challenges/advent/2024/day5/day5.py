with open("day5.txt", "r") as f:
    puzzle = [line.strip() for line in f.readlines()]


rules = []
pages = []
read_rules = True
for line in puzzle:
    if line == "":
        # rules ends with a blank line
        read_rules = False
    if read_rules:
        # extract rules form the input file, and store them
        # rules are in the format "B|A" where B and A are the two numbers specifying order of pages : B before A, not necessarily immediately before
        rules.append(line.split("|"))
    else:
        # extract the pages from the input file and store them
        # pages are in the format "A,B,..." where A, B, ... are the numbers of the pages
        # pages start after the blank line following the rules
        # skip blank line
        if line != "":
            pages.append(line.split(","))

# Create a dictionary to store the order of the pages
order = {}
adjs = {}
for rule in rules:
    # rule[0] is the page that comes before rule[1], add to list of pages that come before rule[1]
    if rule[1] not in order:
        order[rule[1]] = []
    if rule[0] not in adjs:
        adjs[rule[0]] = []
    order[rule[1]].append(rule[0])
    adjs[rule[0]].append(rule[1])

# check if the order of the pages is correct, each line in the pages list should be in the order specified by the rules
# pages[i][j] must come before all pages[i][k] where k > j
result = 0
incorrect_pages = []
for page in pages:
    correct_order = True
    for i in range(len(page)):
        for j in range(i+1, len(page)):
            # check if page[j] comes before page[i] in the order specified by the rules
            if page[i] in order and page[j] in order[page[i]]:
                correct_order = False
                break
        if not correct_order:
            break
    if correct_order:
        # get middle page number
        result += int(page[len(page)//2])
    else:
        incorrect_pages.append(page)

print(result)

result = 0
# fix the incorrect pages
for page in incorrect_pages:
    # find the correct order of the pages
    # print(page)
    correct_order = []

    # sort the pages in the order specified by the rules using topological sort
    def dfs(adj, x, visited, order):
        visited[x] = 1
        for w in adj[x] if x in adj else []:
            if visited[w] == 0 and w in page:
                dfs(adj, w, visited, order)
        order.append(x)


    visited = {k: 0 for k in list(adjs.keys())+list(order.keys())}
    reorder = []
    for w in adjs:
        if visited[w] == 0 and w in page:
            dfs(adjs, w, visited, reorder)
    reorder = reorder[::-1]
    # get middle page number
    result += int(reorder[len(reorder)//2])

print(result)
