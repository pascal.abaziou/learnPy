import re

instr = ""
f = open("day3.txt", "r")

for line in f.readlines():
    instr += line

# search for mul(d+,d+) in the input string
match = re.search(r"mul\((\d+),(\d+)\)", instr)

somme = 0

while match:
    mul = instr[match.start():match.end()]

    somme += int(match.group(1)) * int(match.group(2))

    instr = instr[match.end():]
    match = re.search(r"mul\((\d+),(\d+)\)", instr)

print(somme)
