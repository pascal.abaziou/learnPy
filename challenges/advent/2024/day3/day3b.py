import re

instr = ""
f = open("day3.txt", "r")

for line in f.readlines():
    instr += line

# search for mul(d+,d+) or don't() or do() in the input string
regex = r"mul\((\d+),(\d+)\)|don\'t\(\)|do\(\)"
match = re.search(regex, instr)

# iterate over matches to
# check if the match is found
# or not

somme = 0
add = True

while match:

    mul = instr[match.start():match.end()]
    if mul == "don't()":
        add = False
    elif mul == "do()":
        add = True
    else:
        if add:
            somme += int(match.group(1)) * int(match.group(2))

    instr = instr[match.end():]
    match = re.search(regex, instr)

print(somme)
