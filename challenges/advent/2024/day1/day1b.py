f = open("day1.txt", "r")

l = []
r = []

for line in f.readlines():
    a, b = line.split()
    l.append(a)
    r.append(b)


similarity = 0

for n in l:
    similarity += int(n) * r.count(n)

print(similarity)


