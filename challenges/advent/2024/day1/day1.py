f = open("day1.txt", "r")

l = []
r = []

for line in f.readlines():
    a, b = line.split()
    l.append(a)
    r.append(b)

ll = sorted(l)
rl = sorted(r)

distance = 0

for i in range(len(ll)):
    distance += abs(int(ll[i]) - int(rl[i]))

print(distance)


