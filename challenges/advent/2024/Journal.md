
## Day 4

Thème : détection des mots dans une grille

problem 1 : entièrement résolu par l'IA
problem 2 : comptag revue et nodifié par le développeur

## Day 5

Thème : tri d'un arbre

problem 1 : quasiment résolu par l'IA
problem 2 : tri topologique avec un graphe orienté acyclique mis en place par l'IA
    complété par le développeur pour la résolution du problème :
    - chaque arbre n'était qu'un sous-ensemble des noeuds du graphe
    - gestion des noeuds sans successeurs
