import re
from functools import reduce

f = open("day2.txt", "r")
cal = 0


def val(line):
    """
    12 red cubes, 13 green cubes, and 14 blue

    """
    sets = line.split(':')[1]
    mins = {'red': 0, 'green': 0, 'blue': 0}
    for s in sets.split(';'):
        cubes = re.split(', ', s)
        for cube in cubes:
            ns, c = cube.split()
            n = int(ns)
            # print(c, n)
            if n > mins[c]:
                mins[c] = n
        # print('---')

    i = reduce(lambda x, key: x * mins[key], mins, 1)
    # print(mins, i)
    return i


for line in f.readlines():
    cal += val(line)

print(cal)
