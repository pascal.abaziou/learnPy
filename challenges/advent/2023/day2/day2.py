import re

f = open("day2.txt", "r")
cal = 0



def val(line):
    """
    12 red cubes, 13 green cubes, and 14 blue

    """
    sets = line.split(':')
    for t in re.split('[,;] ', sets[1][1:]):
        ns, c = t.split()
        n = int(ns)
        if n > 14:
            return 0
        if n > 13 and c == 'green':
            return 0
        if n > 12 and c == 'red':
            return 0

    return int(sets[0].split()[1])




for line in f.readlines():
    cal += val(line)

print(cal)
