f = open("day1.txt", "r")
cal = 0


def val(line):
    for c in line:
        if c.isdigit():
            return int(c)


for line in f.readlines():
    cal += val(line) * 10
    cal += val(line[::-1])

print(cal)
