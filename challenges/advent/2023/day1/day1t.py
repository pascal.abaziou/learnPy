f = open("day1.txt")

digits = {
    "one": 1, "two": 2, "three": 3, "four": 4, "five": 5, "six": 6, "seven": 7, "eight": 8, "nine": 9
}

cal = 0


def val(line):
    for i in range(len(line)):
        if line[i].isdigit():
            return int(line[i])
        for k in digits.keys():
            if k in line[0:i+1]:
                return int(digits[k])
    return 0

def vald(line):
    l = len(line)
    for i in reversed(range(l)):
        if line[i].isdigit():
            return int(line[i])
        for k in digits.keys():
            if k in line[i:l]:
                return int(digits[k])
    return 0


for line in f.readlines():
    cal += val(line) * 10
    cal += vald(line)

print(cal)
