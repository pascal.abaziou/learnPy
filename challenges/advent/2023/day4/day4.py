import re

f = open("day4.txt", "r")

lines = f.readlines()

s = 0
for line in lines:
    data = line.split(':')[1][1:]
    w, numbers = [ d.split() for d in data.split('|')]
    #print(w, numbers)
    n = 0
    for num in numbers:
        if num in w:
            n += 1
    if n >= 1:
        points = pow(2, n - 1)
        print(points)
        s += points

print(s)
