import re

f = open("day4.txt", "r")

lines = f.readlines()

t = len(lines)
s = 0
instances = [1 for _ in range(t)]

for line in lines:
    t -= 1
    data = line.split(':')[1][1:]
    w, numbers = [d.split() for d in data.split('|')]
    n = 0
    for num in numbers:
        if num in w:
            n += 1
    score = min(n, t)

    s += instances[0]
    inst = instances[0]
    instances.pop(0)
    for i in range(score):
        instances[i] += inst

print(s)
