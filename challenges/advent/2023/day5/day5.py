import re

f = open("day5.txt", "r")

lines = f.readlines()

cur = [int(c) for c in lines[0].split(':')[1].split()]

lines.pop(0)
lines.pop(0)

print(cur)
print(lines[0])


# last step : humidity-to-location map:
while len(lines):
    step_ranges = []
    print(lines[0][:-1])
    lines.pop(0)
    while len(lines) > 0 and len(lines[0]) > 1:
        # print(lines[0])
        step_ranges.append(list(map(lambda x: int(x), lines[0].split())))
        lines.pop(0)

    next = []
    for c in cur :
        suiv = -1
        for r in step_ranges:
            next_start = r[0]
            seed_start = r[1]
            r = r[2]
            if seed_start <= c < seed_start + r:
                suiv = next_start + c - seed_start
        if suiv == -1:
            suiv = c
        next.append(suiv)
    print(step_ranges)
    print(next)
    cur = next
    if len(lines):
        lines.pop(0)
    print()

print(min(cur))
