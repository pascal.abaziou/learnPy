f = open("day3.txt", "r")

lines = f.readlines()

ll = len(lines[0])
val = 0

gear = [[0 for _ in range(ll)] for _ in range(len(lines))]
val_gear = [[1 for _ in range(ll)] for _ in range(len(lines))]


def voisinage(i, j):
    gears = set()
    for ii in range(-1, 2):
        for jj in range(-1, 2):
            if 0 <= i + ii < len(lines) and 0 <= j + jj < ll:
                v = lines[i + ii][j + jj]
                if v == '*':
                    gears.add((i + ii, j + jj))

    return gears


for i in range(len(lines)):
    cur = ""
    gears_n = set()
    for j in range(ll):
        car = lines[i][j]
        if car.isdigit():
            cur += car
            gears_n.update(voisinage(i, j))
        else:
            if len(cur) > 0:
                for coords in gears_n:
                    val_gear[coords[0]][coords[1]] *= int(cur)
                    gear[coords[0]][coords[1]] += 1
            cur = ""
            gears_n = set()

s = 0

for i in range(len(lines)):
    for j in range(ll):
        if gear[i][j] > 1:
            s += val_gear[i][j]

print(s)
