import re

f = open("day3.txt", "r")

lines = f.readlines()

ll = len(lines[0])
val = 0


def voisinage(i, j):
    symb = False
    for ii in range(-1, 2):
        for jj in range(-1, 2):
            if 0 <= i + ii < len(lines) and 0 <= j + jj < ll:
                v = lines[i + ii][j + jj]
                if not v.isdigit() and not v == '.' and not v == '\n':
                    symb = True
    return symb


for i in range(len(lines)):
    cur = ""
    adj = False
    for j in range(ll):
        car = lines[i][j]
        if car.isdigit():
            cur += car
            adj |= voisinage(i,j)
        else:
            if len(cur) and adj:
                val += int(cur)
                print(cur)
            cur = ""
            adj = False


print(val)

