def solve(b):
    # s = sorted(b)
    score = ''
    curr_cit = 0
    h = 0
    hdict = {}
    for book in b:
        for ab in range(book):
            boo = ab + 1
            if not boo in hdict:
                hdict[boo] = 0
            if boo <= book:
                hdict[boo] = min(boo, hdict[boo] + 1)
                if hdict[boo] > h:
                    h = hdict[boo]
        score += str(h) + " "
    return score


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer

    for case in range(1, t + 1):
        n = map(int, input().split())
        b = list(map(int, input().split()))

        print("Case #{}: {}".format(case, solve(b)))
