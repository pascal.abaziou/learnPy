def solve(s, f):
    sol = 0
    cars = [c for c in s]
    for c in [*s]:
        diff = 30
        for t in f:
            direct_diff = abs(ord(c) - ord(t))
            if direct_diff > 13:
                direct_diff -= 13
            diff = min(diff, direct_diff)
        #     print(direct_diff)
        # print(diff)
        # print("---")
        sol += diff
    return sol


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer
    for case in range(1, t + 1):
        s = input()
        f = input()

        print("Case #{} {}".format(case, solve(s, f)))

