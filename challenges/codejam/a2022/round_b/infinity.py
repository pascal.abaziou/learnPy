import math


def solve(r, a, b, case = 1):
    c = 1
    s = r * r
    while r > 0 :
        r *= a
        s += r * r
        r //= b
        s += r * r

    return s * math.pi


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer
    for case in range(1, t + 1):
        r, a, b = map(int, input().split())

        print("Case #{}: {}".format(case, solve(r, a, b)))

