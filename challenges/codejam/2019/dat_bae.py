import sys


def dicho(bads):
    l = bads[0]
    q = ''
    for _ in range(len(bads) - 1):
        split = l/2
        q += '1' * (l//2)+ '0' * ()


def solve():
    n, b, f = map(int, input().split())

    q = ''

    nb_sections = n//b;
    sections = [ ['1' if i%2 == 0 else '0']*b for i in range(nb_sections)]

    if (n%b) > 0:
        nb_sections += 1
        sections.append(['1' if (n//b)%2 == 0 else '0'] * (n%b))

    bads = [0] * nb_sections

    sections = [[]] * nb_sections

    print("sections : {}".format(nb_sections), file=sys.stderr)
    for s in range(n // b):
        if s%2 == 0:
            q += '1' * b
        else:
            q += '0' * b
        #sections[s] = ([1] * b)

    if n%b > 0:
        if n//b % 2 == 0:
            q+= '1' * (n%b)
        else:
            q+= '0' * (n%b)
        #sections[nb_sections-1] = ([1] * (n%b))


    print(q)

    resp = input()

    bads_cpt = 0
    for i in range(nb_sections):
        res_section = resp[i*b-bads_cpt:(i+1)*b-bads_cpt]
        bads_sec = res_section.count('0' if i%2 == 0 else '1')
        if len(res_section) < b:  # last section with bads
            bads_sec += b - len(res_section)
        print("SEC = {} with {} bads ".format(res_section, bads_sec), file=sys.stderr)

        bads_cpt += bads_sec
        bads[i] = (len(res_section), bads_sec)

    print(bads, file=sys.stderr)

    for i in range(4):
        q = ''
        for ind, b in enumerate(bads):
            if b == 0:
                q += '1' * b
            else:
                q += dicho(bads[i])



if __name__ == '__main__':
    t = int(input())  # number of test cases
    for _ in range(0, t):
        solve()
