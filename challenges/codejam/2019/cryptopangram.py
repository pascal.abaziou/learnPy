import sys


def pgcd(a, b):
    """pgcd(a,b): calcul du 'Plus Grand Commun Diviseur' entre les 2 nombres entiers a et b"""
    if (b > a):
        return pgcd(b, a)
    if b == 0:
        return a
    else:
        r = a % b
        return pgcd(b, r)


if __name__ == '__main__':

    t = int(next(sys.stdin))  # read a line with a single integer

    for k in range(1, t + 1):
        maxInt, l = [int(s) for s in next(sys.stdin).split(" ")]
        cypher = [int(s) for s in next(sys.stdin).split(" ")]

        clear = [0 for _ in range(l + 1)]

        for i in range(0,l):
            if cypher[i] != cypher[i + 1]:
                pivot = pgcd(cypher[i], cypher[i+1])
                clear[i] = cypher[i] // pivot
                clear[i+1] = pivot

                break

        alpha = set()

        alpha.add(clear[i])
        alpha.add(clear[i+1])

        for j in range(i+1, l+1):
            # print("j = ", j)
            clear[j] = cypher[j - 1] // clear[j - 1]
            alpha.add(clear[j])

        for j in range(i-1, -1, -1):
            clear[j] = cypher[j + 1] // clear[j + 1]
            alpha.add(clear[j])

        # clear[l] = cypher[l - 1] // clear[l - 1]
        # alpha.add(clear[l])

        alphabet = sorted([x for x in iter(alpha)])

        cleart = [chr(ord('A') + alphabet.index(c)) for c in clear]
        print("Case #{}: {}".format(k, ''.join(cleart)))
