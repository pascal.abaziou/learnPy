import math
import sys
from collections import Counter


def match(l, resp, i, j):
    result = True
    for idx in range(i):
        # print( "--> *** testing range *** {} {}".format(i, j), file=sys.stderr)
        result &= l[j*5][idx] == resp[idx]
    return result


def solve():
    letters = ['A', 'B', 'C', 'D', 'E']
    l = [['' for _ in range(5)] for _ in range(119)]
    resp = ['' for _ in range(5)]
    for i in range(4):
        # print(math.perm(5-i)-1)
        for j in range(math.perm(5)-1):
            # if i != 0:
            #     print("equals ? {}  -> {}".format(l[j][:i] , resp[:i]), file=sys.stderr)
            if i ==0 or  l[j][:i] == resp[:i] :  #match(l, resp, i-1 ,j):
                # if i != 0:
                    # print("equals ? {}  -> {}".format(l[j][:i], resp[:i]), file=sys.stderr)
                    # print(j*5+1+i, file=sys.stderr)
                print(j*5+1+i)
                l[j][i] = input()
                # print(l[j][i], file=sys.stderr)
        # print(l, file=sys.stderr)
        test_list = Counter([g[i] for g in l])
        # print("{}  {}".format(i, test_list), file=sys.stderr)
        if i == 3:
            letter = test_list.most_common(2)[1][0]
            resp[i+1] = letter
            letters.remove(letter)
            resp[i] = letters[0]

        elif 0 < i < 3 :
            letter = test_list.most_common(5-i+1)[5-i][0]
            resp[i] = letter
            letters.remove(letter)
        else :
            letter = test_list.most_common(5-i)[5-i-1][0]
            resp[i] = letter
            letters.remove(letter)

        # print("remainng : {}".format(letters), file=sys.stderr)


    # resp[4] = letters[0]
    # print("--> *** res *** {} ".format("".join(resp)), file=sys.stderr)
    sys.stderr.flush()
    print("".join(resp))
    input()
    # print(input(), file=sys.stderr)


if __name__ == '__main__':
    t, f = map(int, input().split())
        # number of test cases, number of tries
    for _ in range(0, t):
        solve()
