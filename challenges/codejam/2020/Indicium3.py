import collections
from itertools import permutations
from math import factorial

import numpy as np


def affiche(matrix):
    for l in matrix:
        s = [str(c) for c in l]
        print(' '.join(s))


def trace(m, n):
    t = 0
    for i in range(n):
        t += m[i][i]
    return t


def latin(m, n):
    for i in range(n):
        r = 1 if collections.Counter(m[i]).most_common(1)[0][1] > 1 else 0
        if r == 1:
            return False

    a = np.array(m).transpose()
    for i in range(n):
        c = 1 if collections.Counter(a[i]).most_common(1)[0][1] > 1 else 0
        if c == 1:
            return False

    return True


def diflines(m, numline):
    chiffres = [z + 1 for z in range(n)]
    for sup in range(numline):
        chiffres.remove(m[sup][c])



def makeline(m, numline, n):
    if numline >= n:
        return trace(m, n) == k and latin(m, n)

    for l in diflines(m, numline):
        print(l)

    for c in range(n):
        chiffres = [z+1 for z in range(n)]
        for sup in range(numline):
            chiffres.remove(m[sup][c])
        for chi in chiffres:
            m[numline][c] = chi
            if c==3 and makeline(m, numline+1, n ):
                return True


def solve(m ,n, k, r, c, diag):

    if r > n-1:
        return ("IMPOSSIBLE", m)
    if r == n-1 and c == n and diag == k and latin(m, n):
        return ("POSSIBLE", m)
    if r == n - 1 and c == n:
        return ("IMPOSSIBLE", m)

    if c > n-1:
        res= solve(m,n, k, r+1, 0, diag)

    for i in range(n):
        m[r][c] = i+1
        if c==r:
            diag += i+1

        res = solve(m, n, k, r, c+1, diag)
        if res[0] == "POSSIBLE":
            return res
        m[r][c] = 0
        if c==r:
            diag -= i+1

    return ("IMPOSSIBLE", m)


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer

    for case in range(1, t + 1):
        n, k = map(int, input().split())
        m = [[0 for _ in range(n)] for _ in range(n)]

        if k < n or k > n * n:
            print("Case #{}: {}".format(case, "IMPOSSIBLE"))
        if k % n == 0:
            resp, good_matrix = solve(m, n, k, 0, 0, 0)
            # return possible(diag(n, k / n))
            print("Case #{}: {}".format(case, resp))
            if resp == "POSSIBLE":
                affiche(good_matrix)
        if k == n + 1 or k == n * n - 1 or n < 4:
            print("Case #{}: {}".format(case, "IMPOSSIBLE"))


