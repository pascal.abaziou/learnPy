def solve(n):
    print("1 1")
    if n != 1:
        print("2 1")
    if n == 3:
        print("3 1")
    elif n > 3:
        print("3 2")
    tot = 4
    for i in range(n-4):
        print("{} 1".format(i + 3))
        tot += 1
    print(tot)


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer
    for case in range(1, t + 1):
        n = int(input())

        print("Case #{}:".format(case))
        solve(n)
