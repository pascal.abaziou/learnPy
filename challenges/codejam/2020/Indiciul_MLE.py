import collections
import numpy as np
import itertools

t = int(input())  # read a line with a single integer


def affiche(matrix):
    for l in matrix:
        s = [str(c) for c in l]
        print(' '.join(s))


def trace(m, n):
    t = 0
    for i in range(n):
        t += m[i][i]
    return t


def latin(m, n):
    for i in range(n):
        r = 1 if collections.Counter(m[i]).most_common(1)[0][1] > 1 else 0
        if r == 1:
            return False

    a = np.array(m).transpose()
    for i in range(n):
        c = 1 if collections.Counter(a[i]).most_common(1)[0][1] > 1 else 0
        if c == 1:
            return False

    return True


for case in range(1, t + 1):
    n, k = map(int, input().split())

    bline = [i+1 for i in range(n)]

    matrix = list(itertools.permutations(bline))

    resp = "IMPOSSIBLE"

    all_matrix = list(itertools.permutations(matrix))

    for m in all_matrix:
        mm = m[:n]
        if trace(mm, n) == k and latin(mm, n):
            resp = "POSSIBLE"
            good_matrix = mm
            break


    print("Case #{}: {}".format(case, resp))
    if resp == "POSSIBLE":
        affiche(good_matrix)


