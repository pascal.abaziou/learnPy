def solve(x, y, ms):

    nb = len(m)

    time = 0

    n = ms.count('N')
    s = ms.count('S')
    dy = abs(y + n - s)

    e = ms.count('E')
    w = ms.count('W')
    dx = abs(x + e - w)

    if dx + dy > nb:
        return 'IMPOSSIBLE'
    # else:
    #     return dx + dy

    for d in ms:

        if x == 0 and y == 0:
            return time

        if d == 'S':
            if y == 0:
                y -= 1
                continue
            if y == 1:
                y = 0
            if x == 0:
                if y >= 2:
                    y -= 2
            else:
                x += -1 if x > 0 else 1
        if d == 'N':
            if y == 0:
                y += 1
            elif y == -1:
                pass
            else:
                x += -1 if x > 0 else 1
        time += 1

    return time


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer
    for case in range(1, t + 1):
        x, y, m = input().split()

        print("Case #{} {}".format(case, solve(int(x), int(y), m)))


# 5
# 4 4 SSSS
# 3 0 SNSS
# 2 10 NSNNSN
# 0 1 S
# 2 7 SSSSSSSS


# 2
# 3 2 SSSW
# 4 0 NESW
