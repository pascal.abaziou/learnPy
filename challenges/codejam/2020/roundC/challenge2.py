import math
import sys

D = math.pow(10, 9)


def solve(u):

    sol = [set() for _ in range(10)]
    notsol = [set() for _ in range(10)]

    for _ in range(10000):
        istr, r = input().split()
        i = int(istr)

        if i == 10 and len(r) == 2:
            sol[0].add(r[1])
        if i < 10:
            for j in range(1, 10):
                if j >= i:
                    sol[j].add(r)
                # else:
                #     notsol[j].add(r)

    for j in range(9, 1, -1):
        for k in sol[j-1]:
            sol[j].remove(k)

    return ''.join([s.pop() for s in sol])


if __name__ == '__main__':

    t = int(input())
    u = int(input())
    for case in range(1, t + 1):

        print("Case #{}: {}".format(case, solve(u)))
