def solve(n, activities):
    res = ''
    j, c = (0, 0), (0, 0)
    resseq = ["" for _ in range(n)]
    # print(activities)
    sort = sorted(activities)  # , key= lambda val: val[0])
    # print(sort)
    for act in sort:
        s, e = act[0], act[1]
        if s == e:
            continue
        # if s >= j[1]:
        #     j[0] = 0
        # if s>= c[1]:
        #     c[0] = 0

        if (s < j[1] and s < c[1]):
            return "IMPOSSIBLE"
            # break

        if s >= c[1]:  # c[0] == 0:
            c = (1, e)
            idx = activities.index([s, e])
            if resseq[idx] == '':
                resseq[idx] = "C"
            else:
                idx2 = activities[idx+1:].index([s, e])
                resseq[idx+1+idx2] = "C"
        elif s >= j[1]:  # j[0] == 0:
            j = (1, e)
            idx = activities.index([s, e])
            if resseq[idx] == '':
                resseq[idx] = "J"
            else:
                idx2 = activities[idx+1:].index([s, e])
                resseq[idx+1+idx2] = 'J'

    # if
    # if res != "IMPOSSIBLE":
    # print(resseq)
    return ''.join(resseq)


t = int(input())  # read a line with a single integer

for case in range(1, t + 1):
    n = int(input())

    activities = [[] for _ in range(n)]
    for k in range(n):
        a, b = map(int, input().split())
        activities[k] = [a, b]

    print("Case #{}: {} ".format(case, solve(n, activities)))
