import collections
from copy import deepcopy

import numpy as np


def affiche(matrix):
    for l in matrix:
        s = [str(c) for c in l]
        print(' '.join(s))


def latin(m, n):
    for i in range(n):
        r = 1 if collections.Counter(m[i]).most_common(1)[0][1] > 1 else 0
        if r == 1:
            return False

    a = np.array(m).transpose()
    for i in range(n):
        c = 1 if collections.Counter(a[i]).most_common(1)[0][1] > 1 else 0
        if c == 1:
            return False

    return True


def sol(tab, n, ch, row, col, d, dd):
    if col == n:
        ch = [str(x + 1) for x in range(n)]
        ch.remove(str(d))
        ch.remove(str(dd))
        return sol(tab, n, ch, row + 1, 0, d, dd)
    elif row == n:
        return True
    elif tab[row][col] != '0':
        return sol(tab, n, ch, row, col + 1, d, dd)
    else:
        cand = deepcopy(ch)
        for r in range(row):
            if tab[r][col] in cand:
                cand.remove(tab[r][col])
        for c in range(col):
            if tab[row][c] in cand:
                cand.remove(tab[row][c])
        for car in cand:
            tab[row][col] = str(car)
            ch.remove(car)
            res = sol(tab, n, ch, row, col + 1, d, dd)
            if res:
                return True
            tab[row][col] = '0'
            ch.insert(0, car)


def solve(n, k):
    d = k // n
    r = k % n
    dd = d + 1

    if r == 1 or r == n-1:
        if d == n-1:
            dd = n
            d = d - 2
        else:
            dd = d + 2
            d = d-1
        r = 2
        kk = k - dd * 2
        for crest in reversed(range(n-2)):
            if kk == (crest+1) * d:
                break
            r += 1
            kk -= dd

    # print("{} {}", d, r)
    t = ['0' for _ in range(n)]
    for i in range(n):
        if i < r:
            t[i] = dd # + 1
        else:
            t[i] = d
    # print(t)

    chiffres = [x + 1 for x in range(n)]
    # print(chiffres)
    # print("--------")
    a = deepcopy(chiffres)
    a.remove(d)
    a.remove(dd)
    tab = [['0' for _ in range(n)] for _ in range(n)]
    for row in range(n):
        # print("---> {}".format(row))
        if row < r:
            for c in range(n):
                if c == 0 and row == r - 1:
                    tab[row][c] = str(d)
                elif c == row:
                    tab[row][c] = str(dd)
                elif c == row + 1 and row != r - 1:
                    tab[row][c] = str(d)

        else:
            for c in range(n):
                if c == row:
                    tab[row][c] = str(d)
                elif row < n - 1 and c == row + 1:
                    tab[row][c] = str(dd)
                elif row == n - 1 and c == r:
                    tab[row][c] = str(dd)

    sol(tab, n, a, 0, 0, d, dd)



    # if row < n - 1:
    #     i = tab[row].index(str(d))
    #     i += 1
    #     for c in a:
    #         libre = False
    #         while not libre:
    #             if i == n:
    #                 i = 0
    #             if tab[row][i] == '0':
    #                 libre = True
    #                 break
    #             i += 1
    #         tab[row][i] = str(c)
    #         i += 1
    # else:
    #     for col in range(n):
    #         if tab[row][col] != '0':
    #             cand = deepcopy(chiffres)
    #             for arow in range(n - 1):
    #                 cand.remove(int(tab[arow][col]))
    #             tab[row][col] = str(cand[0])

        # if i < n-1 and tab[row][i+1] != '0':
        #     i += 1
        # i+=1
        # for c in a:
        #     if i < n - 1 and tab[row][i + 1] != '0':
        #         i += 1
        #     if i >= n:
        #         i = 0
        #     tab[row][i] = c
        #     i += 1

        # print(" ".join(l))

    # print("is latin -->  {}", latin(tab, n))
    affiche(tab)
    # print(" ".join(l1) + " " + str(lc) + " " + " ".join(l2))

    pass


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer

    for case in range(1, t + 1):
        n, k = map(int, input().split())
        # print(n, k)
        if n == 2 and k != 3:
            print("Case #{}: {}".format(case, "IMPOSSIBLE"))
        elif k < n or k > n * n:
            print("Case #{}: {}".format(case, "IMPOSSIBLE"))
        elif k == n + 1 or k == n * n - 1:
            print("Case #{}: {}".format(case, "IMPOSSIBLE"))
        else:  # if k % n == 0:
            print("Case #{}: {}".format(case, "POSSIBLE"))
            solve(n, k)
        # elif k == n + 1 or k == n * n - 1 or n < 4:
        #      print("Case #{}: {}".format(case, "IMPOSSIBLE"))
        # else:
        #     print("Case #{}: {}".format(case, "POSSIBLE"))
        #     solve(n, k)
