import collections
import numpy as np

t = int(input())  # read a line with a single integer
for case in range(1, t + 1):
    n = int(input())

    lines = [[] for _ in range(n)]
    k = 0
    r = 0
    c = 0
    for i in range(n):
        lines[i] = [int(s) for s in input().split()]
        r += 1 if collections.Counter(lines[i]).most_common(1)[0][1] > 1 else 0
        k+= lines[i][i]

    a = np.array(lines).transpose()
    # print(a)
    for i in range(n):
        c += 1 if collections.Counter(a[i]).most_common(1)[0][1] > 1 else 0

    print("Case #{}: {} {} {}".format(case, k, r, c))


