def solve(x, y):

    x, y = -x, -y
    j = 1

    seq = ""

    if x % 2 > 0 and y % 2 > 0:
        return "IMPOSSIBLE"

    while True:
        if j > abs(x) and x != 0 or j > abs(y) and y != 0:
            return "IMPOSSIBLE"
        if x == 0 and y == 0:
            return seq

        d = min(abs(x), abs(y))
        if abs(x) < abs(y):



        if j == x:
            seq += "W"
            x -= j
        elif j == -x:
            seq += "E"
            x += j
        elif j == y:
            seq += "S"
            y -= j
        elif j == -y:
            seq += "N"
            y += j


        j *= 2


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer
    for case in range(1, t + 1):
        x, y = map(int, input().split())

        print("Case #{} {}".format(case, solve(x, y)))

