def solve(n):
    # tot = 1
    # print("1 1")       # 1
    # if n != 1:
    #     print("2 1")   # 1
    #     tot += 1
    # if n == 3:
    #     print("3 1")   # 1
    #     tot += 1
    # elif n > 3:
    #     print("3 2")   # 2
    #     tot += 2
    # reste = n - 4

    # lv = [1, 3, 3, 1]
    # print("reste {}".format(reste))
    # l = 4

    print("1 1")
    l = 2
    lv = [1, 1]
    reste = n - 1
    gd = True
    dp = 2

    while reste > 0:
        ls = sum(lv)
        if ls <= reste:
            for i in range(len(lv)):
                print("{} {}".format(l, i+1 if gd else len(lv)-i))
            gd = not gd
            reste -= ls
            lv = [1] + [lv[i] + lv[i+1] for i in range(len(lv)-1)] + [1]
            dp = len(lv)
            # print(lv)
        else:
            lvs = [1] + [lv[i] + lv[i + 1] for i in range(len(lv) - 1)] + [1]

            print("{} {}".format(l, 1 if gd else dp))
            dp += 1
            reste -= 1
            lv = [1] + [lv[i] + lv[i + 1] for i in range(len(lv) - 1)] + [1]
        l += 1

    #     if reste < l - 1:
    #         # print("{} 1".format(l))
    #         print(1)
    #         reste -= 1
    #         tot += 1  ###
    #         l += 1
    #     else:
    #         # print("{} 2".format(l))
    #         print("1 {}".format(l-1))
    #         # print( l - 1 )
    #         reste -= l-1
    #         tot += l-1 ###
    #         if reste >= l:
    #             l += 1
    # if tot != n:
    #     print("ERROR {} -> }".format(n, tot))
    # else:
    #     print("{} at row {}".format(tot, l))


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer
    for case in range(1, t + 1):
        n = int(input())

        print("Case #{}:".format(case))
        solve(n)

    # for test in range(501):
    #     solve(test + 1)
    #
    # solve(1000000)
