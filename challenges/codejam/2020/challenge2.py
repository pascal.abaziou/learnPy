import math
import sys

D = math.pow(10, 9)


def solve(a, b):
    print(0,0)
    res = input()
    if ( res == "CENTER"):
        print("OK", file=sys.stderr)
        return


if __name__ == '__main__':

    t, a, b = map(int, input().split())
    for case in range(1, t + 1):

        solve(a, b)
