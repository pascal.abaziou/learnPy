import collections
from itertools import permutations
from math import factorial

import numpy as np


def affiche(matrix):
    for l in matrix:
        s = [str(c) for c in l]
        print(' '.join(s))

def trace(m, n):
    t = 0
    for i in range(n):
        t += m[i][i]
    return t


def latin(m, n):
    for i in range(n):
        r = 1 if collections.Counter(m[i]).most_common(1)[0][1] > 1 else 0
        if r == 1:
            return False

    a = np.array(m).transpose()
    for i in range(n):
        c = 1 if collections.Counter(a[i]).most_common(1)[0][1] > 1 else 0
        if c == 1:
            return False

    return True


def makeline(m, numline, n, permuts):
    if numline >= n:
        return trace(m, n) == k and latin(m, n)
    for l in permuts:
        m[numline] = l
        if makeline(m, numline+1, n, permuts):
            return True


def solve(n, k):
    m = [[0 for _ in range(n)] for _ in range(n)]

    line = [ i+1 for i in range(n)]
    lines = list(permutations(line))

    makeline(m, 0, n, lines)

    if trace(m, n) == k and latin(m, n):
        return("POSSIBLE", m)

    return ("IMPOSSIBLE", m)


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer

    for case in range(1, t + 1):
        n, k = map(int, input().split())

        resp, good_matrix = solve(n, k)

        print("Case #{}: {}".format(case, resp))
        if resp == "POSSIBLE":
            affiche(good_matrix)
