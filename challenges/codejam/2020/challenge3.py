def solve(n):
    pass


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer
    for case in range(1, t + 1):
        n = int(input())

        print("Case #{}:".format(case))
        solve(n)
