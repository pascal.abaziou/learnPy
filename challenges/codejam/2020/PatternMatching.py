def solve(patterns):
    start, end = '', ''
    for p in patterns:
        if p[0] != '*':
            cstart = p[:p.index('*')]
            if start == '' or cstart.startswith(start):
                start = cstart
            elif start.startswith(cstart):
                pass
            else:
                return '*'
        # print("--->".format(p[-2:]))
        if p[-1] != '*':
            cend = p[p.index('*') + 1:]
            if end == '' or cend.endswith(end):
                end = cend
            elif end.endswith(cend):
                pass
            else:
                return '*'
            # print("--> cend : {}  => {}".format(cend ,end))
    return start + end


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer
    for case in range(1, t + 1):
        n = int(input())
    
        patterns = [input() for _ in range(n)]

        print("Case #{}: {}".format(case, solve(patterns)))
    
    