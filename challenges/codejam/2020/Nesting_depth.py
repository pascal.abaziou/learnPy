import collections
import numpy as np

t = int(input())  # read a line with a single integer
for case in range(1, t + 1):
    s = [int(s) for s in input()]

    r = ""
    curr_par = 0
    # print(s)
    for c in s:
        if c > curr_par:
            r += "(" * (c-curr_par)
            curr_par = c
        elif c< curr_par:
            r += ")" * (curr_par - c)
            curr_par = c
        r += str(c)

    r += ")" * c

    print("Case #{}: {}".format(case, r))

import collections
import numpy as np

t = int(input())  # read a line with a single integer
for case in range(1, t + 1):
    s = [int(s) for s in input()]

    r = ""
    curr_par = 0
    # print(s)
    for c in s:
        if c > curr_par:
            r += "(" * (c-curr_par)
            curr_par = c
        elif c< curr_par:
            r += ")" * (curr_par - c)
            curr_par = c
        r += str(c)

    r += ")" * c

    print("Case #{}: {}".format(case, r))

# 9
# 0000
# 101
# 111000
# 1
# 021
# 312
# 4
# 221
# 4221
