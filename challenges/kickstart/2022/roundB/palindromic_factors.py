import math


def racine(a):
    v = math.sqrt(a)
    if v.is_integer():
        return int(v) + 1
    return math.ceil(v)


def solve(a):
    n = 1
    if a != 1 and is_palindrome(a):
        n += 1

    L = [i for i in range(2, racine(a))]

    for k in L:
        if a % k == 0:
            if is_palindrome(k):
                n += 1
            if k != a//k and is_palindrome(a//k) :
                n += 1

    return n


def is_palindrome(n):
    s = str(n)
    return s == str(s[::-1])


if __name__ == '__main__':
    t = int(input())  # read a line with a single integer

    # f = open('palindromic.txt', 'w')

    for i in range(1, t + 1):
        a = int(input())

        print("Case #{}: {}".format(i, solve(a)))


