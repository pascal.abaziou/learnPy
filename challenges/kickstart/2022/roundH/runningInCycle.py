def solve(l, n):
    runs = [ input().split() for _ in range(n)]


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer
    for case in range(1, t + 1):
        n, b = [int(s) for s in input().split()]

        print("Case #{}: {}".format(case, solve(l, n)))
