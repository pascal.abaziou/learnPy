def solve(n, k, p):

    stack = [[_ for _ in range(k)] for _ in range(n)]
    for i in range(n):
        s = [int(s) for s in input().split()]
        for j, v in enumerate(s):
            stack[i][j] = (stack[i][j-1] if j>0 else 0) + v

    sol = [[0 for _ in range(p)] for _ in range(n)]
    for i in range(n):
        for j in range(p):
            for x in range(min(j, k)):
                sol[i][j] = max([sol[i][j], stack[i][x] + sol[i-1][j-x]])

    return sol[n-1][p-1]


if __name__ == '__main__':

    t = int(input())  # read a line with a single integer
    for case in range(1, t + 1):
        n, k, p = [int(s) for s in input().split()]

        print("Case #{}: {}".format(case, solve(n, k, p)))

