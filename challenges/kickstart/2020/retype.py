t = int(input())  # read a line with a single integer
for i in range(1, t + 1):
    n, k, s = map(int, input().split())

    if k - s < s:
        y = n + 2 * (k - s)
    else:
        y = k + n

    print("Case #{}: {}".format(i, y))
