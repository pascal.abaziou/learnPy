t = int(input())

for i in range(t):
    n, m = [int(s) for s in input().split()]
    s = min(n, m)
    if s == 0:
        print("Case #{}: {}".format(i + 1, 0))
    else :
        print("Case #{}: {}".format(i + 1, s + s - 1))
