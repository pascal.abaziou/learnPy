t = int(input())

for i in range(t):
    n, m = [int(s) for s in input().split()]
    c = sum([int(s) for s in input().split()])
    print("Case #{}: {}".format(i + 1, c % m))

