from typing import List, Any


def all_the_same(elements: List[Any]) -> bool:
    # your code here
    cur = None
    for e in elements:
        if cur is not None and e != cur:
            return False
        cur = e
    return True


if __name__ == '__main__':
    print("Example:")
    print(all_the_same([1, 1, 1]))

    # These "asserts" are used for self-checking and not for an auto-testing
    assert all_the_same([1, 1, 1])
    assert all_the_same([1, 2, 1]) is False
    assert all_the_same(['a', 'a', 'a']) is True
    assert all_the_same([]) is True
    assert all_the_same([1]) is True
    print("Coding complete? Click 'Check' to earn cool rewards!")
