# -*- coding: utf-8 -*-
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats


def predict(x):
    return slope * x + intercept


if __name__ == '__main__':
    # df = pd.read_csv("univariate_linear_regression_dataset.csv")
    df = pd.read_csv("mco.csv")
    #print len(df)
    X = df.iloc[0:len(df),0] #selection de la première colonne de notre dataset
    Y = df.iloc[0:len(df),1]

    axes = plt.axes()
    #axes.set_xlim([2, 25])
    #axes.set_ylim([0, 30])
    axes.grid()
    plt.scatter(X,Y)

    # plt.show()
    #plt.savefig("D:/figure.png")

    slope, intercept, r_value, p_value, std_err = stats.linregress(X, Y)

    #print r_value * r_value
    fitLine = predict(X)
    plt.plot(X, fitLine, c='r')

    plt.show()
    plt.savefig("./mco.png")

    print("f(x) = {} x + {}".format(slope, intercept))

    print(predict(20.27))