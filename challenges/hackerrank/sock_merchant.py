#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the sockMerchant function below.


def sockMerchant(n, ar):
    pairs = 0
    oddSocks = set()
    for s in ar:
        if s in oddSocks:
            pairs += 1
            oddSocks.remove(s)
        else:
            oddSocks.add(s)
    return pairs


if __name__ == '__main__':
#    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    ar = list(map(int, input().rstrip().split()))

    result = sockMerchant(n, ar)

    print(str(result) + '\n')

    # fptr.write(str(result) + '\n')

    # fptr.close()
