#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the countingValleys function below.
def countingValleys(n, s):
    steps = [c for c in s]
    print(steps)
    level = 0
    valleys = 0
    for c in s:
        if level == 0 and c == 'D':
            valleys += 1
        if c == 'D':
            level -= 1
        else:
            level +=1
    return valleys


if __name__ == '__main__':
    # fptr = open(os.environ['OUTPUT_PATH'], 'w')

    n = int(input())

    s = input()

    result = countingValleys(n, s)

    print(result)

    # fptr.write(str(result) + '\n')
    #
    # fptr.close()
