deplacement = {
    'r': (1, 0),
    'l': (-1, 0),
    'u': (0, -1),
    'd': (0, 1)

}


def next_position(pos, dec):
    return pos[0] + dec[0], pos[1] + dec[1]


def valid_position(candidate_position, visited):
    return 0 <= candidate_position[0] < 5 and 0 <= candidate_position[1] < 5 \
           and visited[candidate_position[1]][candidate_position[0]] != 1


def actual_path(pos, str, visited, actual):
    visited[pos[1]][pos[0]] = 1

    if pos == (4, 4) and len(str) == 0:
        return actual

    if len(str) == 0:
        visited[pos[1]][pos[0]] = 0
        return None

    c = str[0]

    if c != '?':
        next_pos = next_position(pos, deplacement[c])
        if not valid_position(next_pos, visited):
            visited[pos[1]][pos[0]] = 0
            return None
        candidate_path = actual_path(next_pos, str[1:], visited, actual + c)
        if candidate_path is None:
            visited[pos[1]][pos[0]] = 0
        return candidate_path

    for movement in deplacement:
        candidate_position = next_position(pos, deplacement[movement])
        if valid_position(candidate_position, visited):
            candidate_path = actual_path(candidate_position, str[1:], visited, actual + movement)
            if candidate_path is not None:
                return candidate_path

    visited[pos[1]][pos[0]] = 0
    return None


def correct_path(str):

    pos = (0, 0)
    visited = [[0 for _ in range(5)] for _ in range(5)]
    real_path = actual_path(pos, str, visited, '')

    return real_path


if __name__ == '__main__':
    # keep this function call here
    print(correct_path(input()))
