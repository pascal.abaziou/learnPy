def first_factorial(num):
    # code goes here
    fact = 1
    for i in range(2, num+1):
        fact = fact * i
    return fact


# keep this function call here
print(first_factorial(int(input())))

