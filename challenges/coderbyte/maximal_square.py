import numpy as np


def MaximalSquare(strArr):
    # code goes here

    square = [[int(c) for c in l] for l in strArr.translate(str.maketrans('', '', '"[] ')).split(',')]
    # python 2 : square = [[int(c) for c in l] for l in strArr]
    res = mSquare(np.asarray(square))
    return res


def mSquare(square):
    dimx = len(square)
    dimy = len(square[0])
    dim = min(dimx, dimy)

    if np.all(square, None):
        return dim * dim

    if dim == 1:
        return 1

    if dimx < dimy:
        ma = 1
        for i in range(dimy - dimx + 1):
            s = mSquare(square[0:dimx, i: dimx + i])
            if s > ma:
                ma = s
        return ma

    if dimx > dimy:
        ma = 1
        for i in range(dimx - dimy + 1):
            s = mSquare(square[i:dimy+i, 0: dimy])
            if s > ma:
                ma = s
        return ma

    return max(
        max(
            mSquare(square[0:dimx - 1, 0:dimy - 1]),
            mSquare(square[1: dimx, 1: dimy])
        ),
        max(
            mSquare(square[0: dimx - 1, 1: dimy]),
            mSquare(square[1: dimx, 0: dimy - 1])
        ))


if __name__ == '__main__':
    # keep this function call here
    print(MaximalSquare(input()))
