def traveling(x1, y1, x2, y2):
    if x1 == x2 or y1 == y2:
        return 1

    return traveling(x1, y1, x2 - 1, y2) + traveling(x1, y1, x2, y2 - 1)


def chessboard_traveling(str):
    # code goes here

    pos = str.replace("(", '').replace(")", ' ')
    [x1, y1, x2, y2] = [int(p) for p in pos[:-1].split(" ")]

    return traveling(x1, y1, x2, y2)


if __name__ == '__main__':
    # keep this function call here
    print(chessboard_traveling(input()))
