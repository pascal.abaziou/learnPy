def PentagonalNumber(num):
    # code goes here
    if num == 1:
        return 1
    return (num - 1) * 5 + PentagonalNumber(num - 1)

# => passage en O(1) : démonstration
#   (num - 1) * 5 + PentagonalNumber(num - 1)
# = (num - 1) * 5 + 1 + 5 * (num - 2) * (num-1) / 2
# = 1 + (num - 1) * 5 + 5 * (num - 2) * (num-1) / 2
# = 1 + 5 * (num - 1)  + 5 * (num - 2) * (num-1) / 2
# = 1 + 5 * ((num - 1) + (num - 2) * (num-1) / 2)
# = 1 + 5 * (2*(num - 1)/2 + (num - 2) * (num-1) / 2)
# = 1 + 5 * (2*(num - 1) + (num - 2) * (num-1)) / 2)
# = 1 + 5 * ((2 + (num - 2)) * (num-1)) / 2)
# = 1 + 5 * ((2 + num - 2) * (num-1)) / 2)
# = 1 + 5 * ((num) * (num-1)) / 2)
# = 1 + 5 * (num - 1) * num / 2


if __name__ == '__main__':

    # keep this function call here
    print(PentagonalNumber(int(input())))
