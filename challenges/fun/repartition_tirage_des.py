from random import *

import matplotlib.pyplot as plt


def de():
    return randint(1, 10)


def tirages(n):
    liste_tirages = {v: 0 for v in range(2,21)}
    for _ in range(n):
        liste_tirages[de()+de()] += 1
    return liste_tirages


nb = 1000 #0000
d = tirages(nb)
plt.plot(d.keys(), [v / nb * 100 for v in d.values()])
print(max([v / nb * 100 for v in d.values()]))
print(min([v / nb * 100 for v in d.values()]))
plt.show()
