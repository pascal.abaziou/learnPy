n = int(input())
d = list(map(int, input().split()))

s = 4 * d[0]
p = d[0]

for c in d[1:]:
    s += 3 * c
    if c > p:
        s += c - p
    p = c

print(s)
