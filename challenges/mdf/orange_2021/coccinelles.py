n = int(input())
d = [[int(c) for c in input().split()] for _ in range(n)]

nbc = 0
m = 0
c = []

v = [0 for _ in range(n)]


def voisin(c1, c2):
    return \
        (c2[0] == c1[0] + c1[2] or c1[0] == c2[0] + c2[2]) and c2[1] < c1[1] + c1[3] and  c1[1] < c2[1] + c2[3] or \
        (c1[1] == c2[1] + c2[3] or c2[1] == c1[1] + c1[3]) and c1[0] < c2[0] + c2[2] and c1[0] + c1[2] > c2[0]


for i in range(n):
    for j in range(i+1,n):
        if voisin(d[i], d[j]):
            v[i] += 1
            v[j] += 1

m = max(v)
c = [idx + 1 for idx, nb in enumerate(v) if nb == m]
nbc = len(c)

print(nbc, m)
print(*c)
