N, D = map(int, input().split())
T1, T2 = (list(map(int, input().split())) for _ in range(2))

# références inverse pour performances
inv_T2 = [None]*N
for i, v in enumerate(T2):
    inv_T2[v-1] = i

c1 = T1[:T1.index(D)]

s = []
for c in c1:
    if inv_T2[c-1] < inv_T2[D-1]:
        s.append(c)

s.append(D)
print(*s)

