from collections import Counter

p = int(input())
m = [list(map(int, input().split())) for _ in range(p)]

n = p // 2
mc = []

for l in m[n//2:n//2 + n]:
    mc.extend(l[n//2:n//2 + n])

mcs = list(sorted(mc))

#print(mc)
#print(mcs)

c = Counter(mcs)

_, top_freq = c.most_common(1)[0]
lower_most_common = min(key for key, freq in c.items() if freq == top_freq)
print(min(mc), max(mc),
      '{0:.1f}'.format((mcs[n//2] + mcs[n//2 + 1]) / 2),
      lower_most_common)
