# Uses python3

import sys


def dfs(adj):  # , used, order, x):
    clock = 0

    def explore(x):
        nonlocal clock
        visited[x] = 1
        clock = clock + 1
        pre[x] = clock
        for w in adj[x]:
            if visited[w] == 0:
                explore(w)
        clock = clock + 1
        post[x] = clock # (x, clock)

    for w in range(len(adj)):
        if visited[w] == 0:
            explore(w)


def toposort(adj):
    used = [0] * len(adj)
    global visited, post, pre
    visited = [0] * len(adj)
    pre = [0] * len(adj)
    post = [0] * len(adj)
    dfs(adj)
#    sort = sorted(post, key=lambda p: p[1], reverse=True)
#    order = [p[0] for p in sort]
    r = max (post)
    res = post.index(r)
    # order = []
    # write your code here
    return res


def solve_toposort():
    # data = list(map(int, input.split()))
    # m, n = data[0:2]
    m, n = [int(s) for s in input().split()]
    adj = [[] for _ in range(n)]
    for _ in range(m):
        a, b = [ int(s) for s in input().split()]
        adj[a].append(b)
    return toposort(adj)


if __name__ == '__main__':
    # input = sys.stdin.read()
    # m, n = [int(s) for s in input().split()]
    # s = input()
    n, m = map(int, input().split())

    order = solve_toposort()
    print(order)
    # for x in order:
    #     print(x + 1, end=' ')
