p = int(input())

for _ in range(4):
    if p % 3 == 0:
        p //= 3
    elif p % 2 == 0:
        p //= 2
    else:
        p -= 1

print(p)
