import itertools

l = [int(c) for c in input().split()]
c = [int(c) for c in input().split()]

candidates = list(itertools.permutations([1, 2, 3, 4, 5, 6, 7, 8, 9]))

for a in candidates:
    if sum(a[0:3]) == l[0] and sum(a[3:6]) == l[1] and sum(a[6:9]) == l[2] \
        and a[0] + a[3] + a[6] == c[0] \
        and a[1] + a[4] + a[7] == c[1] \
        and a[2] + a[5] + a[8] == c[2] :
        print("{}{}{}".format(a[0], a[1], a[2]))
        print("{}{}{}".format(a[3], a[4], a[5]))
        print("{}{}{}".format(a[6], a[7], a[8]))
        break
