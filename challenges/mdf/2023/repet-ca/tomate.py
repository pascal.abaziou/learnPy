
r = [ c for c in input() ]

n = int(input())


def rotate(l, n):
    return l[n:] + l[:n]


for _ in range(n):
    ok = False
    a = [ c for c in input() ]
    for i in range(len(r)):
        if rotate(a, i) == r:
            ok = True
            break
    if ok:
        print("OUI")
    else:
        print("NON")
