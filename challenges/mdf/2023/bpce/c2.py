n = int(input())

nb = 0
for _ in range(n):
    p = 0
    for c in input():
        if c == '.':
            p += 1
        else:
            p = 0
        if p == 4:
            nb += 1
            p = 0

print(nb)
