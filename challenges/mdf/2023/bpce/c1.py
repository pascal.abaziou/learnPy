n = int(input())


t = 0
nf = ""
for _ in range(n):
    nom, d, v = input().split()
    temps = int(d) / int(v)
    if temps > t:
        nf = nom
        t = temps

print(nf)
