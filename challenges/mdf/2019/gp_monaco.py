f = int(input())
fp = f
n = int(input())

pos = [i + 1 for i in range(20)]

for _ in range(n):
    pc, e = input().split()
    pil = int(pc)
    if pil == f and e == 'I':
        print('KO')
        pos[pil - 1] = 21
    else:
        if e == 'I':
            pos = [p - 1 if p > pos[pil-1] else p for p in pos]
            pos[pil - 1] = 21
        else:
            act = pos[pil - 1]
            pos[pos.index(act - 1)] += 1
            pos[pil - 1] -= 1

if pos[f-1] < 21 :
    print(pos[f-1])
