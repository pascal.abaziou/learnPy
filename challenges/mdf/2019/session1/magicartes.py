n = int(input())

s = [int(c) for c in input().split(' ')]
m = [int(c) for c in input().split(' ')]

if max(s) > max(m):
    print('P')
elif max(m) > max(s):
    print('G')
else:
    while len(s) > 0 and len(m) > 0:
        if s[0] > m[0]:
            m = m[1:]
            s = s[1:] + s[0:1]
        elif m[0] > s[0]:
            m = m[1:] + m[0:1]
            s = s[1:]
        else:
            m = m[1:]
            s = s[1:]

    if len(s) == 0 and len(m) == 0:
        print('N')
    elif len(s) > 0:
        print('P')
    else:
        print('G')

