n = int(input())

sol = []

for _ in range(n):
    m = input()
    p = m[::-1]
    if p == m:
        sol.append(m)

print(' '.join(sol))
