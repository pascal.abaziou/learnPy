n = int(input())

maxi = 0
cur = 0

for _ in range(n):
    c = input()
    if c == 'X':
        maxi = max(maxi,cur)
        cur = 0
    else:
        cur += 1

maxi = max(maxi,cur)

print(maxi-1)
