import copy as copy
import numpy as np

lineso = []

for _ in range(8):
    lineso.append([c for c in input()])

colso = np.transpose(lineso)

for idx, l in enumerate(lineso):
    if 'R' in l:
        kl = idx

for idx, c in enumerate(colso):
    if 'R' in c:
        kc = idx

ok = False

for i in [-1, 0, 1]:
    for j in [-1, 0, 1]:

        lines = copy.deepcopy(lineso)
        cols = copy.deepcopy(colso)

        kl_i = kl + i
        kl_i = min(max(0, kl_i), 7)
        kc_j = kc + j
        kc_j = min(max(0, kc_j), 7)

        if kl_i == kl and kc_j == kc:
            # on n'a pas bougé, risque de PAT
            continue

        # on déplace le roi qui prend éventuellement une tour
        lines[kl_i][kc_j] = 'R'
        cols[kc_j][kl_i] = 'R'

        if not ('T' in lines[kl_i] or 'T' in cols[kc_j]):
            ok = True

if ok:
    print('still-in-game')
elif 'T' in lineso[kl] or 'T' in colso[kc]:
    print('check-mat')
else:
    print('pat')
