import sys

def tetris(t):
    cols_forb = set()
    col = -1
    n = 0
    for l in t:
        if len(cols_forb) == 10:
            print('NOPE')
            return
        if l.count('#') == 9: # and l.index('.') not in cols_forb:
            if l.index('.') == col:
                n += 1
                if n == 4:
                    print('BOOM', col + 1)
                    return
            elif col == -1:
                col = l.index('.')
                n = 1
            else:
                print('NOPE')
                return
        else:
            col = -1
            n = 0
        for idx, p in enumerate(l):
            if p == '#':
                cols_forb.add(idx)
    print('NOPE')


tableau = [[c for c in input()] for _ in range(20)]

# print("************", file=sys.stderr)
# for t in tableau:
#     print(''.join(t) , file=sys.stderr)


tetris(tableau)


