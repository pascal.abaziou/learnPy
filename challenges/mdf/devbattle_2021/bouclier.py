# solution de la plateforme

n, a, c = map(int, input().split())
ls = list(map(int, input().split()))

dp = [0 for i in range(n+a+c+1)]

absorb = sum(ls[:a])
for i in range(n):
    dp[i+1] = max(dp[i+1], dp[i])
    dp[i+a+c] = max(dp[i+a+c+1], dp[i] + absorb)
    absorb += ls[i+a] if i+a < len(ls) else 0
    absorb -= ls[i]

print(sum(ls) - max(dp))
