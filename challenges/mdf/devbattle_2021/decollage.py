from collections import Counter

n = int(input())
lines = [input() for _ in range(n)]

counter = Counter(lines)

for w, c in counter.items():
    if c == 2:
        print(w)
