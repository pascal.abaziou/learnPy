# avec aide de la solution de la plateforme

from collections import Counter

n = int(input())
d = [c for c in input()]

expected = {k: v/2 for k,v in Counter(d).items()}

m = n // 2
half = Counter(d[:m])

s = 0
for i in range(m):

    if half == expected:
        s += 1
    half[d[i]] -= 1
    half[d[m+i]] += 1

print(s*2)
