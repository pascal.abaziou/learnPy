from collections import Counter

n = int(input())

d = {}

reqs = []

for _ in range(n):
    reqs.append(input())

cmax = 0
m = ""
for r in reqs:

    cur = []
    for i in range(len(r)-9):
        ngram = r[i:i+10]
        c = 0

        for r2 in reqs:
            if ngram in r2:
                c += 1

        if c > cmax:
            cmax = c
            m = ngram
        elif c == cmax:
            if ngram < m:
                m = ngram


print(m, cmax)
