tt = input()

t = int(tt[:-1])

s1 = (20, 1000)
s2 = (100, 500)
s3 = (200, 300)

# print(t)

c1 = 1000 * t + 20 * (100 - t)
c2 = 500 * t + 100 * (100 - t)
c3 = 300 * t + 200 * (100 - t)

# print (c1, c2, c3)

if c1 < c2 and c1 < c3:
    print(1)
elif c2 < c3:
    print(2)
else:
    print(3)

