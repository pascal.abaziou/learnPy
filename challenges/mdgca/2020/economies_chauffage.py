import sys

n = int(input())
# voy, villes = list(map(int, input().split()))

finprev = 8 * 60
eco = 0

lines = []
for _ in range(n):
    lines.append(input().rstrip('\n'))

liness = list(sorted(lines))

for l in liness:
    d, f = l.split("-")
    dh, dm = list(map(int, d.split(":")))
    fh, fm = list(map(int, f.split(":")))
    dnext = dh * 60 + dm
    fnext = fh * 60 + fm

    if dnext - finprev >= 60:
        eco += dnext - finprev

    finprev = fnext

fj = 20 * 60
if fj - finprev >= 60:
    eco += fj - finprev

print(eco)
