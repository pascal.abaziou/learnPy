import sys

voy, villes = list(map(int, input().split()))
rep = {}
surplus = 1
for _ in range(voy):
    nom,d,a = input().split()
    if (d,a) in rep.keys():
        if len(rep[(d,a)]) == 3:
            rep[(surplus, surplus)] = rep[(d,a)]
            surplus += 1
            rep[(d, a)] = []
        rep[(d,a)] = rep.get((d,a)) + [nom]
    else:
        rep[(d,a)] = [nom]


print(len(rep))
for k,l in rep.items():
    for n in l:
        print(n, end=" ")

    print()



