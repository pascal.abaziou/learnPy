n = int(input())
mxskip = int(input())
ls = [int(input()) for _ in range(n)]

dp = [0]
for e in ls:
    dp.append(e + min(dp[-mxskip - 1:]))

print(sum(ls) - min(dp[-mxskip - 1:]))