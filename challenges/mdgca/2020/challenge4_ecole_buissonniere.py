n = int(input())  # nombre de cours
maxc = int(input())  # nombre de cours cons'ecutifs manquables

durees = [int(input()) for _ in range(n)]

sol = [[0 for _ in range(maxc+1)] for _ in range(n + 1)]

for i in range(1,n+1):
    sol[i][0] = max(sol[i-1])
    for c in range(1,maxc+1):
        sol[i][c] = durees[i-1] + sol[i-1][c-1]

print(max(sol[n]))
