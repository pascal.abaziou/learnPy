n = int(input())  # nombre de cours
maxc = int(input())  # nombre de cours cons'ecutifs manquables

durees = []
for _ in range(n):
    # for line in sys.stdin:
    durees.append(int(input()))

sol = [[(0,0) for _ in range(n+1)] for _ in range(n + 1)]

for i in range(1,n+1):
    for c in range(1,n+1):
        inclus = durees[i-1] + sol[i-1][c-1][0]
        capa = sol[i-1][c-1][1]

        exclus = sol[i-1][c][0]

        if inclus > exclus and capa < maxc:
            sol[i][c] = (inclus, capa + 1)
        else:
            sol[i][c] = (exclus, 0)

        # if sol[i][c-1][0] == sol[i][c][0]:
        #     sol[i][c] = (sol[i][c][0], min(sol[i][c-1][1], sol[i][c][1]))
        #
        if sol[i][c-1][0] > sol[i][c][0]:
            sol[i][c] = sol[i][c-1]


for i in range(n+1):
    print(sol[i])

print(sol[n][n])