

def sol(dur, n, maxa):
    if len(dur) == 0:
        return 0

    if n == 0:
        return dur[0] + sol(dur[1:], maxa, maxa)

    return min(
        dur[0] + sol(dur[1:], maxa, maxa),
        sol(dur[1:], n-1, maxa)
    )


n = int(input()) # nombre de cours
maxc = int(input()) # nombre de cours cons'ecutifs manquables

durees = []
for _ in range(n):
    # for line in sys.stdin:
    durees.append(int(input()))

nm = 0
tm = 0

max = sum(durees)

res = sol(durees, maxc, maxc)
print(max-res)

#print(maxc, durees)
