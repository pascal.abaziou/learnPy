import math


def sqr(e):
    return e *e


def cons(q):
    for k in range(4):
        if q[(1+k) %4] * q[(1+k) %4] + sqr(q[(2+k)%4] - q[k]) == sqr(q[(3+k)%4]):
            return True
    return False


def est_triangle (a, b, c):
    return a + b > c and  a + c > b and b+c > a


def un_seul(q):
    for k in range(4):
        if est_triangle(math.sqrt(q[k]*q[k] + sqr(q[(1+k)%4])), q[(2+k)%4], q[(3+k)%4]):
            return True
    return False


def quadrilatere(q):
    return max(q) <= sum(q) - max(q)


def solve(q):    
    if q[0] == q[2] and q[1] == q[3]:
        return(4)
    if q[0]*q[0] + q[1]*q[1] == q[2]*q[2] + q[3]*q[3] or q[0]*q[0] + q[3]*q[3] == q[2]*q[2] + q[1]*q[1]:
        return(2)
    if cons(q):
        return(2)
    if un_seul(q):
        return(1)
    if quadrilatere(q):
        return(0)
    return(-1)


if __name__ == '__main__':
    n = int(input())
    data = [list(map(int, input().split())) for _ in range(n)]

    for q in data:
        print(solve(q))
