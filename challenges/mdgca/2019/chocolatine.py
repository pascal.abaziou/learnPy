def somme_iteree(t):
    si = 0
    while t > 0:
        si += t % 10
        t = t // 10
    return si


def candidate(t):
    if t < 100:
        return t == 42
    else:
        return candidate(somme_iteree(t))


n = int(input())
s = 0
for _ in range(n):
    h = int(input())
    if candidate(h):
        s += 1
print(s)
