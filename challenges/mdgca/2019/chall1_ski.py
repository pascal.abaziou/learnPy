n, m = map(int, input().split())

piste = [[c for c in input()] for _ in range(n)]
sol = [[0 for _ in range(m)] for _ in range(n)]

for c in range(m):
    sol[0][c] = -1 if piste[0][c] == 'X' else 0

for l in range(1, n):
    for c in range(m):
        if piste [l][c] == 'X':
            sol[l][c] = -1
        elif piste[l-1][c] == '.' :
            sol[l][c] = sol[l-1][c] + 1
    for c in range(m-1):
        if sol[l][c] > 0 and piste[l][c+1] == '.' and (sol[l][c] < sol[l][c+1] or sol[l][c+1] == 0):
            sol[l][c + 1] = sol[l][c] + 1
    for c in range(m-1, 0, -1):
        if sol[l][c] > 0 and piste[l][c-1] == '.' and (sol[l][c] < sol[l][c-1] or sol[l][c-1] == 0):
            sol[l][c - 1] = sol[l][c] + 1
    for c in range(m):
        if sol[l][c] == 0:
            sol[l][c] = -1


dist = [s for s in sol[n - 1] if s > 0]
print(min(dist) if len(dist) > 0 else -1)
