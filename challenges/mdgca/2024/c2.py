import numpy as np

y = int(input())
j = int(input())
q = int(input())

ind = y // 2

hu = 0
ch = 0
oi = 0


def calculate_animals(y, j, q):
    # Coefficients matrix
    A = np.array([[2, 2, 2], [2, 4, 2], [0, 1, 1]])

    # Results vector
    B = np.array([y, j, q])

    # Solve the system of equations
    solutions = np.linalg.solve(A, B)

    # Check if all solutions are integers and non-negative
    if np.all(np.isclose(solutions, solutions.astype(int))) and np.all(solutions >= 0):
        return solutions.astype(int)
    else:
        return np.array([])

solution = calculate_animals(y, j, q)

if (len(solution) == 0 ):
    print("Hallucination")
else:
    print(int(solution[0]))
    print(int(solution[1]))
    print(int(solution[2]))
# print(hu)
# print(ch)
# print(oi)
