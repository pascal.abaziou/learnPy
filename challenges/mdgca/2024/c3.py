import copy


def find_cycle(network):
    next_state = network.copy()
    previous_states = [([''.join(str(r)) for r in next_state], 0)]
    step = 0

    while True:
        step += 1
        previous_state = copy.deepcopy(next_state)
        for i in range(h):
            for j in range(w):
                if previous_state[i][j] == 1:  # Add this condition for cells in state 1
                    # Check the state of the neighboring cells
                    neighbors = [(i - 1, j), (i + 1, j), (i, j - 1), (i, j + 1)]
                    neighbors = [(x, y) for x, y in neighbors if
                                 0 <= x < h and 0 <= y < w]
                    state_5_neighbors = sum(1 for x, y in neighbors if previous_state[x][y] == 5)
                    state_1_neighbors = sum(1 for x, y in neighbors if previous_state[x][y] == 1)

                    if state_5_neighbors >= 1:
                        next_state[i][j] = 5
                    elif state_1_neighbors >= 2:
                        next_state[i][j] = 1
                    else:
                        next_state[i][j] = 3

                elif previous_state[i][j] == 5:
                    next_state[i][j] = 4
                elif previous_state[i][j] == 4:
                    next_state[i][j] = 3
                elif previous_state[i][j] == 3:
                    next_state[i][j] = 2
                elif previous_state[i][j] == 2:
                    next_state[i][j] = 1

        jo = [''.join(str(r)) for r in next_state]

        for previous_state, previous_step in previous_states:
            if jo == previous_state:
                print(previous_step)
                print(step - previous_step)
                return

            # Add the current state to the list of previous states
        previous_states.append((jo, step))


w, h = map(int, input().split())

reseau = [[0 for _ in range(w)] for _ in range(h)]

for i in range(h):
    l = input()
    for j in range(w):
        reseau[i][j] = int(l[j])

find_cycle(reseau)
