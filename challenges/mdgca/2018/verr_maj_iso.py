shift_cost = 7
capslock_cost = 13
letter_cost = 10

n = int(input())
s = input()

t = {True: [0 for i in range(n+1)], False: [0 for i in range(n+1)]}
t[True][0] = capslock_cost

for i in range(n):
    for b in {True, False}:
        t[b][i+1] = letter_cost
        if s[i].isupper() == b:
            t[b][i+1] += min(t[b][i], t[not b][i] + capslock_cost)
        else:
            t[b][i+1] += min(t[b][i] + shift_cost,
                             t[not b][i] + capslock_cost)

print(t)
print(min(t[True][n], t[False][n]))
