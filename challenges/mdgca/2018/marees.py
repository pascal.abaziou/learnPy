n = int(input())
s = 14 * 60

m = [ (0,0) for _ in range(n)]

for i in range(n):
    datas= input().split()
    c = int(datas[0])
    h = float(datas[1])
    m[i] = (c, h)

ms = sorted(m, key=lambda x: x[0])

hc = ms[0][1]
for v in ms[1:]:
    if v[1] < hc:
        print("KO")
        exit(0)
    hc = v[1]
print("OK")
