import numpy

n = int(input())
d = input()

dessin = [ ['.' for _ in range(11)] for _ in range(11)]

for i in range(n):
    for j in range(5-i,5+i+1):
        dessin[i][j] = '*'

r = range(11)

if d in ('E', 'O'):
    dessin = [[dessin[j][i] for j in range(len(dessin))] for i in range(len(dessin[0]))]

if d in ('S'):
    r = reversed(r)

if d == 'E':
    for p in r:
        print(''.join(reversed(dessin[p])))

else:
    for p in r:
        print(''.join(dessin[p]))

