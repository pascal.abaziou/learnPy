n = int(input())
s = 14 * 60

for _ in range(n):
    t, ds = input().split()
    d = int(ds)
    if t == "S":
        s -= d

print(s)
