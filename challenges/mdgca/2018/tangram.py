import itertools

local_print = print

ts = 10

n = int(input())
raw = [input().split() for _ in range(n)]

data = [(d[0], int(d[1]), int(d[2])) for d in raw]

perms = itertools.permutations(data)

for perm in perms:
    local_print("perm = ",  perm)
    t = [[False for _ in range(ts)] for _ in range(ts)]
    top = 0
    left = 0
    sol = {} # [ () for _ in range(n)]
    elig = True
    for p in data:
        local_print("piece", p)
        if top + p[1] <= ts and left + p[2] <= ts:
            sub = [[ t[e][f] for f in range(left, left + p[2])] for e in range(top, top + p[1]) ]
        if top + p[1] <= ts and left + p[2] <= ts and True not in sub:
            for i in range(p[1]):
                for j in range(p[2]):
                    t[top+i][left+j] = True
            sol[p[0]] = (top, left)
            for idx, l in enumerate(t):
                if False in l:
                    top = idx
                    break
            for idx, c in enumerate(t[top]):
                if not c:
                    left = idx
                    break
        else:
            elig = False
            break

        local_print(sol)

    if elig:
        local_print("eligible")
        break

local_print(sol)
for p, d in sol.items():
    print(p, d[0], d[1])




