modifies = []
modifies.append(input())
n = int(input())
recompile = []

for _ in range(n):
    cible, ndeps = input().split()
    deps = input().split()
    for f in deps:
        if f in modifies:
            modifies.append(cible)
            recompile.append(cible)
            break

print(len(recompile))
for r in recompile:
    print(r)
