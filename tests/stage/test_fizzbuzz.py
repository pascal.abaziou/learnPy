import unittest


def fizzbuzz(nombre):
    if nombre % 3 == 0:
        return "Fizz"
    if nombre % 5 == 0:
        return "Buzz"
    return str(nombre)


class TestFizzBuzz(unittest.TestCase):

    def test_1_is_1(self):
        self.assertEqual(fizzbuzz(1), "1")

    def test_2_is_2(self):
        self.assertEqual(fizzbuzz(2), "2")

    def test_4_is_4(self):
        self.assertEqual(fizzbuzz(4), "4")

    def test_3_is_Fizz(self):
        self.assertEqual(fizzbuzz(3), "Fizz")

    def test_6_is_Fizz(self):
        self.assertEqual(fizzbuzz(6), "Fizz")

    def test_5_is_Buzz(self):
        self.assertEqual(fizzbuzz(5), "Buzz")

    def test_9_is_Fizz(self):
        self.assertEqual(fizzbuzz(9), "Fizz")


if __name__ == '__main__':
    unittest.main()
