import unittest

from challenges.mdgca.a2022.challenge4 import solve


class TestMdfMethods(unittest.TestCase):

    def test_quadrilatere(self):
        self.assertEqual(solve(list(map(int, "12 9 13 4".split()))), 2)
        self.assertEqual(solve(list(map(int, "43 16 14 4".split()))), -1)
        self.assertEqual(solve(list(map(int, "3 26 20 5".split()))), 0)
        self.assertEqual(solve(list(map(int, "41 49 48 47".split()))), 1)
        self.assertEqual(solve(list(map(int, "47 2 49 14".split()))), 2)
        self.assertEqual(solve(list(map(int, "47 40 47 40".split()))), 4)

        self.assertEqual(solve(list(map(int, "40 30 41 39".split()))), 2)
        self.assertEqual(solve(list(map(int, "40 2 1 21".split()))), -1)
        self.assertEqual(solve(list(map(int, "17 36 15 4".split()))), 0)
        self.assertEqual(solve(list(map(int, "44 33 23 23".split()))), 1)
        self.assertEqual(solve(list(map(int, "35 19 42 30".split()))), 2)
        self.assertEqual(solve(list(map(int, "12 15 12 15".split()))), 4)

        self.assertEqual(solve(list(map(int, "48 12 13 37".split()))), 2)
        self.assertEqual(solve(list(map(int, "26 7 1 15".split()))), -1)
        self.assertEqual(solve(list(map(int, "38 18 3 19".split()))), 0)
        self.assertEqual(solve(list(map(int, "16 15 40 47".split()))), 1)
        self.assertEqual(solve(list(map(int, "37 22 43 2".split()))), 2)
        self.assertEqual(solve(list(map(int, "14 25 14 25".split()))), 4)

        self.assertEqual(solve(list(map(int, "41 10 49 6".split()))), 2)
        self.assertEqual(solve(list(map(int, "47 8 18 8".split()))), -1)
        self.assertEqual(solve(list(map(int, "8 27 44 13".split()))), 0)
        self.assertEqual(solve(list(map(int, "35 36 30 28".split()))), 1)
        self.assertEqual(solve(list(map(int, "11 11 3 3".split()))), 2)
        self.assertEqual(solve(list(map(int, "46 28 46 28".split()))), 4)

        self.assertEqual(solve(list(map(int, "22 20 38 12".split()))), 2)
        self.assertEqual(solve(list(map(int, "23 2 46 8".split()))), -1)
        self.assertEqual(solve(list(map(int, "44 19 23 4".split()))), 0)
        self.assertEqual(solve(list(map(int, "6 44 29 17".split()))), 1)
        self.assertEqual(solve(list(map(int, "14 9 9 14".split()))), 2)
        self.assertEqual(solve(list(map(int, "2 4 2 4".split()))), 4)


if __name__ == '__main__':
    unittest.main()
