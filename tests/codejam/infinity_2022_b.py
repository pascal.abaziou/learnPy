import unittest

from challenges.codejam.a2022.round_b.infinity import solve

if __name__ == '__main__':       
    unittest.main()


def IsApproximatelyEqual(x, y, epsilon):
    """Returns True if y is within relative or absolute 'epsilon' of x.
    """
    # Check absolute precision.
    if -epsilon <= x - y <= epsilon:
        return True
    
    # Is x or y too close to zero?
    if -epsilon <= x <= epsilon or -epsilon <= y <= epsilon:
        return False
    
    # Check relative precision.
    return (-epsilon <= (x - y) / x <= epsilon
      or -epsilon <= (x - y) / y <= epsilon)


class TestMdfMethods(unittest.TestCase):

    def test_running(self):
        self.assertTrue(IsApproximatelyEqual(solve(1, 1, 2), 6.283185307180, 6))
        self.assertTrue(IsApproximatelyEqual(solve(1, 1, 1000), 6.283185307180, 6))
        self.assertTrue(IsApproximatelyEqual(solve(1, 500, 1000), 785401.304990101899, 6))
        self.assertTrue(IsApproximatelyEqual(solve(100000, 1, 2), 83775786506.997749798000, 6))
        self.assertTrue(IsApproximatelyEqual(solve(100000, 1, 1000), 62831915903.648936562240, 6))
        # self.assertTrue(IsApproximatelyEqual(solve(100000, 500, 100), 10472015201267972.222656250000, 6))
        self.assertTrue(IsApproximatelyEqual(solve(100, 25, 50), 26185771.670275108705, 6))
        self.assertTrue(IsApproximatelyEqual(solve(54342, 336, 763, 7), 1299332444762902.580566406250, 6))
        self.assertTrue(IsApproximatelyEqual(solve(41206, 213, 728), 264669111130925.686477661133, 6))
        self.assertTrue(IsApproximatelyEqual(solve(84479, 75, 154), 165356286613137.290481567383, 6))
        self.assertTrue(IsApproximatelyEqual(solve(15672, 61, 285), 3009773896387.345111846924, 6))
        self.assertTrue(IsApproximatelyEqual(solve(67076, 106, 863), 161263138969087.310699462891, 6))
        self.assertTrue(IsApproximatelyEqual(solve(45275, 69, 888), 30852102365113.697372436523, 6))
        self.assertTrue(IsApproximatelyEqual(solve(57511, 32, 899), 10664146570018.969944953918, 6))
        self.assertTrue(IsApproximatelyEqual(solve(62433, 105, 623), 138966475750945.599304199219, 6))
        self.assertTrue(IsApproximatelyEqual(solve(18534, 282, 611), 109048536971320.759284973145, 6))
        self.assertTrue(IsApproximatelyEqual(solve(582, 110, 460), 13654887179.628342159092, 6))
        self.assertTrue(IsApproximatelyEqual(solve(97864, 476, 991), 8861737140876668.781738281250, 6))
        self.assertTrue(IsApproximatelyEqual(solve(12666, 86, 237), 4293277245846.947058916092, 6))
        self.assertTrue(IsApproximatelyEqual(solve(81624, 156, 500), 564322549757153.252990722656, 6))
        self.assertTrue(IsApproximatelyEqual(solve(63240, 216, 829), 628898589999860.406005859375, 6))
        self.assertTrue(IsApproximatelyEqual(solve(94005, 321, 935), 3242864995355355.268310546875, 6))
        self.assertTrue(IsApproximatelyEqual(solve(92004, 169, 536), 843380871108296.219787597656, 6))
        self.assertTrue(IsApproximatelyEqual(solve(17542, 285, 730), 92641355417190.476676940918, 6))
        self.assertTrue(IsApproximatelyEqual(solve(17863, 3, 535), 10024720652.746392738074, 6))
        self.assertTrue(IsApproximatelyEqual(solve(25244, 324, 928), 239332429501692.988769531250, 6))
        self.assertTrue(IsApproximatelyEqual(solve(14207, 8, 588), 41223811133.605344787240, 6))
        self.assertTrue(IsApproximatelyEqual(solve(59886, 240, 794), 714229700938293.332275390625, 6))
        self.assertTrue(IsApproximatelyEqual(solve(38643, 53, 500), 13332291280075.571491241455, 6))
        self.assertTrue(IsApproximatelyEqual(solve(38195, 203, 427), 244019415891410.635253906250, 6))
        self.assertTrue(IsApproximatelyEqual(solve(44227, 446, 895), 1626140792916778.876586914062, 6))
        self.assertTrue(IsApproximatelyEqual(solve(98094, 389, 791), 6033588055693415.691406250000, 6))
        self.assertTrue(IsApproximatelyEqual(solve(55097, 446, 933), 2458870439852447.908447265625, 6))
        self.assertTrue(IsApproximatelyEqual(solve(57692, 265, 879), 807713571580537.653442382812, 6))
        self.assertTrue(IsApproximatelyEqual(solve(95690, 316, 696), 3618369663414228.390136718750, 6))
        self.assertTrue(IsApproximatelyEqual(solve(58186, 60, 138), 47228670177547.708938598633, 6))
        self.assertTrue(IsApproximatelyEqual(solve(86296, 106, 271), 310379109072682.760162353516, 6))
        self.assertTrue(IsApproximatelyEqual(solve(54470, 413, 960), 1950954239712909.231323242188, 6))
        self.assertTrue(IsApproximatelyEqual(solve(83615, 126, 308), 418817872081161.758361816406, 6))
        self.assertTrue(IsApproximatelyEqual(solve(28591, 6, 855), 95023612984.912276215851, 6))
        self.assertTrue(IsApproximatelyEqual(solve(4843, 488, 984), 23264823632210.852668762207, 6))
        self.assertTrue(IsApproximatelyEqual(solve(88487, 333, 786), 3324402067727566.311523437500, 6))
        self.assertTrue(IsApproximatelyEqual(solve(1139, 300, 643), 468525338119.374066412449, 6))
        self.assertTrue(IsApproximatelyEqual(solve(35481, 451, 935), 1048334410647886.426696777344, 6))
        self.assertTrue(IsApproximatelyEqual(solve(79490, 90, 846), 162650744663862.907531738281, 6))
        self.assertTrue(IsApproximatelyEqual(solve(23770, 321, 658), 240024881439898.683654785156, 6))
        self.assertTrue(IsApproximatelyEqual(solve(62479, 253, 578), 971033238885307.976135253906, 6))
        self.assertTrue(IsApproximatelyEqual(solve(37626, 286, 826), 413347582940812.495391845703, 6))
        self.assertTrue(IsApproximatelyEqual(solve(6512, 315, 746), 16084916918281.772494316101, 6))
        self.assertTrue(IsApproximatelyEqual(solve(14261, 109, 949), 7693073436192.568857192993, 6))
        self.assertTrue(IsApproximatelyEqual(solve(52696, 474, 970), 2574835588548535.492675781250, 6))
        self.assertTrue(IsApproximatelyEqual(solve(60231, 492, 990), 3663574405247781.716552734375, 6))
        self.assertTrue(IsApproximatelyEqual(solve(14046, 182, 840), 21542064563510.821926116943, 6))
        self.assertTrue(IsApproximatelyEqual(solve(20435, 395, 848), 261393703542673.744338989258, 6))
        self.assertTrue(IsApproximatelyEqual(solve(73041, 124, 884), 262896208932999.112762451172, 6))
        self.assertTrue(IsApproximatelyEqual(solve(17362, 279, 986), 80129505973964.453910827637, 6))
        self.assertTrue(IsApproximatelyEqual(solve(5687, 479, 996), 30324479365647.611623764038, 6))
        self.assertTrue(IsApproximatelyEqual(solve(59799, 158, 816), 291381614893141.513427734375, 6))
        self.assertTrue(IsApproximatelyEqual(solve(56041, 171, 509), 325218871337410.504608154297, 6))
        self.assertTrue(IsApproximatelyEqual(solve(9206, 468, 966), 76195986385719.381950378418, 6))
        self.assertTrue(IsApproximatelyEqual(solve(3425, 255, 847), 2635101810654.077105283737, 6))
        self.assertTrue(IsApproximatelyEqual(solve(8847, 220, 736), 13068512944851.261168479919, 6))
        self.assertTrue(IsApproximatelyEqual(solve(97774, 497, 997), 9871328384094460.477539062500, 6))
        self.assertTrue(IsApproximatelyEqual(solve(39505, 120, 495), 75014404484656.980537414551, 6))
        self.assertTrue(IsApproximatelyEqual(solve(93237, 403, 832), 5795030901227168.857421875000, 6))
        self.assertTrue(IsApproximatelyEqual(solve(75437, 86, 431), 137726681500932.461509704590, 6))
        self.assertTrue(IsApproximatelyEqual(solve(50307, 318, 656), 1050968097484229.150268554688, 6))
        self.assertTrue(IsApproximatelyEqual(solve(54042, 149, 506), 223044877717233.174926757812, 6))
        self.assertTrue(IsApproximatelyEqual(solve(92773, 56, 455), 86126397893999.973007202148, 6))
        self.assertTrue(IsApproximatelyEqual(solve(31265, 232, 709), 185109300376563.351165771484, 6))
        self.assertTrue(IsApproximatelyEqual(solve(57864, 330, 698), 1475228927776109.613891601562, 6))
        self.assertTrue(IsApproximatelyEqual(solve(25427, 185, 774), 73729102459360.243507385254, 6))
        self.assertTrue(IsApproximatelyEqual(solve(36076, 101, 905), 42239089203132.225154876709, 6))
        self.assertTrue(IsApproximatelyEqual(solve(65764, 138, 840), 265943475185945.277557373047, 6))
        self.assertTrue(IsApproximatelyEqual(solve(22459, 390, 959), 288779097694961.618957519531, 6))
        self.assertTrue(IsApproximatelyEqual(solve(61034, 30, 350), 10622340266622.171292304993, 6))
        self.assertTrue(IsApproximatelyEqual(solve(73017, 349, 707), 2697352685103313.877441406250, 6))
        self.assertTrue(IsApproximatelyEqual(solve(85964, 300, 850), 2386746686646013.324707031250, 6))
        self.assertTrue(IsApproximatelyEqual(solve(3055, 492, 998), 9374341015619.865383148193, 6))
        self.assertTrue(IsApproximatelyEqual(solve(97652, 220, 533), 1747742931889332.375000000000, 6))
        self.assertTrue(IsApproximatelyEqual(solve(56046, 12, 822), 1431197732520.186971426010, 6))
        self.assertTrue(IsApproximatelyEqual(solve(16075, 45, 931), 1648558851681.440586924553, 6))
        self.assertTrue(IsApproximatelyEqual(solve(70150, 477, 972), 4633372425201080.216796875000, 6))
        self.assertTrue(IsApproximatelyEqual(solve(21099, 9, 372), 114746984857.738834403455, 6))
        self.assertTrue(IsApproximatelyEqual(solve(5097, 275, 746), 7141917337111.490833282471, 6))
        self.assertTrue(IsApproximatelyEqual(solve(27184, 489, 988), 735209108410705.862426757812, 6))
        self.assertTrue(IsApproximatelyEqual(solve(9379, 349, 865), 40203206752450.741142272949, 6))
        self.assertTrue(IsApproximatelyEqual(solve(50037, 322, 899), 935567558742587.750244140625, 6))
        self.assertTrue(IsApproximatelyEqual(solve(44027, 263, 900), 460539346444480.722930908203, 6))
        self.assertTrue(IsApproximatelyEqual(solve(140, 353, 822), 9372667169.619077451527, 6))
        self.assertTrue(IsApproximatelyEqual(solve(72047, 247, 745), 1117765533514925.521362304688, 6))
        self.assertTrue(IsApproximatelyEqual(solve(40289, 367, 828), 854749004298550.458801269531, 6))
        self.assertTrue(IsApproximatelyEqual(solve(87455, 251, 781), 1688173764471014.293701171875, 6))
        self.assertTrue(IsApproximatelyEqual(solve(44519, 341, 710), 941084973268575.644531250000, 6))
        self.assertTrue(IsApproximatelyEqual(solve(36844, 137, 703), 83207357256184.195091247559, 6))
        self.assertTrue(IsApproximatelyEqual(solve(94350, 240, 493), 2111201541007343.551269531250, 6))
        self.assertTrue(IsApproximatelyEqual(solve(45043, 20, 276), 2569413053453.721036672592, 6))
        self.assertTrue(IsApproximatelyEqual(solve(28315, 394, 912), 480708263365993.305877685547, 6))
        self.assertTrue(IsApproximatelyEqual(solve(44055, 496, 998), 1992073772222633.461059570312, 6))
        self.assertTrue(IsApproximatelyEqual(solve(99266, 411, 999), 6294623803476731.677246093750, 6))
