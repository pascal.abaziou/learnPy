
Feature: Maximal Square
Have the function MaximalSquare(strArr) take the strArr parameter being passed which will be a 2D matrix of 0 and 1's,
and determine the area of the largest square submatrix that contains all 1's. A square submatrix is one of equal width
and height, and your program should return the area of the largest submatrix that contains only 1's. For example:
if strArr is ["10100", "10111", "11111", "10010"] then this looks like the following matrix:

1 0 1 0 0
1 0 1 1 1
1 1 1 1 1
1 0 0 1 0

For the input above, you can see the bolded 1's create the largest square submatrix of size 2x2, so your program
should return the area which is 4. You can assume the input will not be empty.


5. For the input ["1111", "1111", "1111"] your output was incorrect. The correct answer is 9.
6. For the input ["1111", "1101", "1111", "0111"] your output was incorrect. The correct answer is 4.
8. For the input ["01001", "11111", "01011", "11111", "01111", "11111"] your output was incorrect. The correct answer is 9.
10. For the input ["101101", "111111", "011111", "111111", "001111", "011111"] your output was incorrect. The correct answer is 16.

  Scenario: Matrix ["10100", "10111", "11111", "10010"]
    Given the matrix ["10100", "10111", "11111", "10010"]
    When the maximal square is computed
    Then the surface is 4

  Scenario: Matrix ["0111", "1111", "1111", "1111"]
    Given the matrix ["0111", "1111", "1111", "1111"]
    When the maximal square is computed
    Then the surface is 9

  Scenario: Matrix ["0111", "1101", "0111"]
    Given the matrix ["0111", "1101", "0111"]
    When the maximal square is computed
    Then the surface is 1

  Scenario: Matrix ["1111", "1111"]
    Given the matrix ["1111", "1111"]
    When the maximal square is computed
    Then the surface is 4

  Scenario: Matrix ["01001", "11111", "01011", "11011"]
    Given the matrix ["01001", "11111", "01011", "11011"]
    When the maximal square is computed
    Then the surface is 4

  Scenario: Matrix ["101101", "111111", "010111", "111111"]
    Given the matrix ["101101", "111111", "010111", "111111"]
    When the maximal square is computed
    Then the surface is 9

  Scenario: Matrix ["01001", "11111", "01011", "11111", "01111", "11111"]
    Given the matrix ["01001", "11111", "01011", "11111", "01111", "11111"]
    When the maximal square is computed
    Then the surface is 9


