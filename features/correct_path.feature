Feature: Correct Path
  Have the function CorrectPath(str) read the str parameter being passed, which will represent the movements made in a
  5x5 grid of cells starting from the top left position. The characters in the input string will be entirely composed
  of: r, l, u, d, ?. Each of the characters stand for the direction to take within the grid, for example: r = right,
  l = left, u = up, d = down. Your goal is to determine what characters the question marks should be in order for a
  path to be created to go from the top left of the grid all the way to the bottom right without touching previously
  travelled on cells in the grid.

  For example: if str is "r?d?drdd" then your program should output the final correct string that will allow a path to
  be formed from the top left of a 5x5 grid to the bottom right. For this input, your program should therefore return
  the string rrdrdrdd. There will only ever be one correct path and there will always be at least one question mark
  within the input string.

  1. For the input "r?d?drdd" the correct answer is rrdrdrdd.
  2. For the input "???rrurdr?" the correct answer is dddrrurdrd.
  3. For the input "drdr??rrddd?" the correct answer is drdruurrdddd.
  4. For the input "rrrr????" the correct answer is rrrrdddd.
  5. For the input "ddd?uru??ddd" the correct answer is dddrururrddd.
  6. For the input "rd?u??dld?ddrr" the correct answer is rdrurrdldlddrr.
  7. For the input "dddd?uuuurrr????" the correct answer is ddddruuuurrrdddd.
  8. For the input "ddr?rdrrd?dr" the correct answer is ddrurdrrdldr.
  9. For the input "rdrdrdr?" the correct answer is rdrdrdrd.
  10. For the input "rdrdr??rddd?dr" the correct answer is rdrdruurdddldr.

  Scenario: 1 - path "r?d?drdd"
    Given the path "r?d?drdd"
    When correct path is searched
    Then path "rrdrdrdd" is retrieved

  Scenario: 2 - path "???rrurdr?"
    Given the path "???rrurdr?"
    When correct path is searched
    Then path "dddrrurdrd" is retrieved

  Scenario: 3 - path "drdr??rrddd?"
    Given the path "drdr??rrddd?"
    When correct path is searched
    Then path "drdruurrdddd" is retrieved

  Scenario: 6 - path "rd?u??dld?ddrr"
    Given the path "rd?u??dld?ddrr"
    When correct path is searched
    Then path "rdrurrdldlddrr" is retrieved


