from behave import *

from challenges.coderbyte.kaprekars_constant import kaprekars_constant


@step("Le nombre {number:d}")
def step_impl(context, number):
    context.number = number


@step("on appelle la fonction KaprekarsConstant")
def step_impl(context):
    context.actual = kaprekars_constant(context.number)


@step("le nombre d'itérations est {expected:d}")
def step_impl(context, expected):
    assert context.actual == expected

