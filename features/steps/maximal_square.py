from behave import *

from challenges.coderbyte.maximal_square import MaximalSquare


@when("the maximal square is computed")
def step_impl(context):
    context.actual = MaximalSquare(context.matrix)


@given('the matrix {matrix}')
def step_impl(context, matrix):
    context.matrix = matrix


@then("the surface is {expected:d}")
def step_impl(context, expected):
    assert context.actual == expected
