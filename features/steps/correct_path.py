from behave import *
from hamcrest import assert_that, equal_to

from challenges.coderbyte.correct_path import correct_path


@given('the path "{path}"')
def step_impl(context, path):
    context.path = path


@when("correct path is searched")
def step_impl(context):
    context.actual = correct_path(context.path)


@then('path "{expected}" is retrieved')
def step_impl(context, expected):
    assert_that(context.actual, equal_to(expected), "Should find the correct path")
