from behave import *

from challenges.coderbyte.chessboard_traveling import chessboard_traveling


@given('les positions {positions}')
def step_impl(context, positions):
    context.positions = positions


@when("je détermine le nombre de chemins")
def step_impl(context):
    context.actual = chessboard_traveling(context.positions)


@then("le nombre de chemins possible est {expected:d}")
def step_impl(context, expected):
    assert context.actual == expected


