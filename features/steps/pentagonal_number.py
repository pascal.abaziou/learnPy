from behave import *

from challenges.coderbyte.pentagonal_number import PentagonalNumber


@given("the iteration {iteration:d}")
def step_impl(context, iteration):
    context.iteration = iteration


@when("calculationg total of dots")
def step_impl(context):
    context.actual = PentagonalNumber(context.iteration)


@then("the total number of dots is {expected:d}")
def step_impl(context, expected):
    assert context.actual == expected
