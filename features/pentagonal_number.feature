Feature: Pentagonal number
Have the function PentagonalNumber(num) read num which will be a positive integer and determine how many dots exist in
a pentagonal shape around a center dot on the Nth iteration. For example, in the image below you can see that on the
first iteration there is only a single dot, on the second iteration there are 6 dots, on the third there are 16 dots,
and on the fourth there are 31 dots.

Your program should return the number of dots that exist in the whole pentagon on the Nth iteration.

  Scenario: iteration 1
    Given the iteration 1
    When calculationg total of dots
    Then the total number of dots is 1

  Scenario: iteration 2
    Given the iteration 2
    When calculationg total of dots
    Then the total number of dots is 6

  Scenario: iteration 3
    Given the iteration 3
    When calculationg total of dots
    Then the total number of dots is 16

  Scenario: iteration 4
    Given the iteration 4
    When calculationg total of dots
    Then the total number of dots is 31

  Scenario: iteration 5
    Given the iteration 5
    When calculationg total of dots
    Then the total number of dots is 51

